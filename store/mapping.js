// import config from '~/store/config'
import {encryptAPI, decryptAPI, showNotification} from '../plugins/global-function'
export const state = () => ({
	tempImageFile: null,
	dataAddUsers: null,
	fileProgressPercentage: 0
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		console.log('server init')
	},
	//region MAPPING DISASTER
	async downloadXlsxDisasterMapping ({ commit }, payload) {
		// console.log(payload)
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/mapping/generate-xlsx`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteDisasterMpaiing ({ commit }, payload) {
		return await this.$axios.delete('cms/mapping/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	filePercentage(state, payload){
		state.fileProgressPercentage = payload
	},
	savedStateAddUsers(state, payload){
		// console.log(payload)
		state.dataUsers = payload
	},
	setTempImageFile(state, payload){
		state.tempImageFile = payload
		state.dataAddUsers.image_url = payload.name
		// console.log(state.tempImageFile)
	},
	setTempImageFileContent(state, payload){
		state.tempImageFile = payload
		state.dataUsers.image = payload.name
		// console.log(state.tempImageFile)
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},

}


