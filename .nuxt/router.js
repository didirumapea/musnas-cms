import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _33707d72 = () => interopDefault(import('../pages/admin/index.vue' /* webpackChunkName: "pages/admin/index" */))
const _69cd2302 = () => interopDefault(import('../pages/banner/index.vue' /* webpackChunkName: "pages/banner/index" */))
const _0c3a701c = () => interopDefault(import('../pages/content/index.vue' /* webpackChunkName: "pages/content/index" */))
const _49aaa312 = () => interopDefault(import('../pages/dashboard/index.vue' /* webpackChunkName: "pages/dashboard/index" */))
const _0d51b5f4 = () => interopDefault(import('../pages/disaster/index.vue' /* webpackChunkName: "pages/disaster/index" */))
const _090977b4 = () => interopDefault(import('../pages/klausul-disclaimer/index.vue' /* webpackChunkName: "pages/klausul-disclaimer/index" */))
const _1b038a16 = () => interopDefault(import('../pages/login_page.vue' /* webpackChunkName: "pages/login_page" */))
const _3391ae31 = () => interopDefault(import('../pages/mapping/index.vue' /* webpackChunkName: "pages/mapping/index" */))
const _49c24eb1 = () => interopDefault(import('../pages/order/index.vue' /* webpackChunkName: "pages/order/index" */))
const _417df5c6 = () => interopDefault(import('../pages/organization/index.vue' /* webpackChunkName: "pages/organization/index" */))
const _1fef025f = () => interopDefault(import('../pages/ticket/index.vue' /* webpackChunkName: "pages/ticket/index" */))
const _2e31b4eb = () => interopDefault(import('../pages/users/index.vue' /* webpackChunkName: "pages/users/index" */))
const _7feae24b = () => interopDefault(import('../pages/version-control/index.vue' /* webpackChunkName: "pages/version-control/index" */))
const _5c680ed6 = () => interopDefault(import('../pages/banner/image_cropper.vue' /* webpackChunkName: "pages/banner/image_cropper" */))
const _1bc75842 = () => interopDefault(import('../pages/components/accordion.vue' /* webpackChunkName: "pages/components/accordion" */))
const _13f8c573 = () => interopDefault(import('../pages/components/alert.vue' /* webpackChunkName: "pages/components/alert" */))
const _b6fc0f30 = () => interopDefault(import('../pages/components/animations.vue' /* webpackChunkName: "pages/components/animations" */))
const _edd5b8de = () => interopDefault(import('../pages/components/avatars.vue' /* webpackChunkName: "pages/components/avatars" */))
const _81800ba2 = () => interopDefault(import('../pages/components/badge_label.vue' /* webpackChunkName: "pages/components/badge_label" */))
const _737de58a = () => interopDefault(import('../pages/components/base.vue' /* webpackChunkName: "pages/components/base" */))
const _0ce968fc = () => interopDefault(import('../pages/components/breadcrumb.vue' /* webpackChunkName: "pages/components/breadcrumb" */))
const _08887d58 = () => interopDefault(import('../pages/components/buttons.vue' /* webpackChunkName: "pages/components/buttons" */))
const _5d93c7fa = () => interopDefault(import('../pages/components/cards.vue' /* webpackChunkName: "pages/components/cards" */))
const _67d86594 = () => interopDefault(import('../pages/components/color_palette.vue' /* webpackChunkName: "pages/components/color_palette" */))
const _6bb9fa2b = () => interopDefault(import('../pages/components/drop_dropdowns.vue' /* webpackChunkName: "pages/components/drop_dropdowns" */))
const _63051ca0 = () => interopDefault(import('../pages/components/fab_buttons.vue' /* webpackChunkName: "pages/components/fab_buttons" */))
const _3c5b94d2 = () => interopDefault(import('../pages/components/filters.vue' /* webpackChunkName: "pages/components/filters" */))
const _4ff97b14 = () => interopDefault(import('../pages/components/footer.vue' /* webpackChunkName: "pages/components/footer" */))
const _25e1b102 = () => interopDefault(import('../pages/components/grid.vue' /* webpackChunkName: "pages/components/grid" */))
const _5422b0a0 = () => interopDefault(import('../pages/components/height.vue' /* webpackChunkName: "pages/components/height" */))
const _194e8031 = () => interopDefault(import('../pages/components/icons.vue' /* webpackChunkName: "pages/components/icons" */))
const _2338db2c = () => interopDefault(import('../pages/components/lists.vue' /* webpackChunkName: "pages/components/lists" */))
const _e1928bc8 = () => interopDefault(import('../pages/components/masonry.vue' /* webpackChunkName: "pages/components/masonry" */))
const _dbd6ed6a = () => interopDefault(import('../pages/components/modals_dialogs.vue' /* webpackChunkName: "pages/components/modals_dialogs" */))
const _f42d8802 = () => interopDefault(import('../pages/components/notifications.vue' /* webpackChunkName: "pages/components/notifications" */))
const _023fcd13 = () => interopDefault(import('../pages/components/pagination.vue' /* webpackChunkName: "pages/components/pagination" */))
const _5cecbe3a = () => interopDefault(import('../pages/components/progress_spinners.vue' /* webpackChunkName: "pages/components/progress_spinners" */))
const _c0c7d6c0 = () => interopDefault(import('../pages/components/scrollable.vue' /* webpackChunkName: "pages/components/scrollable" */))
const _3d3c2b5a = () => interopDefault(import('../pages/components/slider.vue' /* webpackChunkName: "pages/components/slider" */))
const _1985729e = () => interopDefault(import('../pages/components/sortable.vue' /* webpackChunkName: "pages/components/sortable" */))
const _baa6e244 = () => interopDefault(import('../pages/components/tables.vue' /* webpackChunkName: "pages/components/tables" */))
const _b5d41892 = () => interopDefault(import('../pages/components/tabs.vue' /* webpackChunkName: "pages/components/tabs" */))
const _16f97b0c = () => interopDefault(import('../pages/components/timeline.vue' /* webpackChunkName: "pages/components/timeline" */))
const _46d2875c = () => interopDefault(import('../pages/components/toolbar.vue' /* webpackChunkName: "pages/components/toolbar" */))
const _a8ec80ee = () => interopDefault(import('../pages/components/tooltips.vue' /* webpackChunkName: "pages/components/tooltips" */))
const _6a92c4d6 = () => interopDefault(import('../pages/components/transitions.vue' /* webpackChunkName: "pages/components/transitions" */))
const _681124dd = () => interopDefault(import('../pages/components/width.vue' /* webpackChunkName: "pages/components/width" */))
const _1ef02373 = () => interopDefault(import('../pages/content/image_cropper.vue' /* webpackChunkName: "pages/content/image_cropper" */))
const _e3be7ab4 = () => interopDefault(import('../pages/dashboard/v1.vue' /* webpackChunkName: "pages/dashboard/v1" */))
const _e3a24bb2 = () => interopDefault(import('../pages/dashboard/v2.vue' /* webpackChunkName: "pages/dashboard/v2" */))
const _b69fdd6a = () => interopDefault(import('../pages/disaster/image_cropper.vue' /* webpackChunkName: "pages/disaster/image_cropper" */))
const _75c26de9 = () => interopDefault(import('../pages/forms/dynamic_fields.vue' /* webpackChunkName: "pages/forms/dynamic_fields" */))
const _430e5aea = () => interopDefault(import('../pages/forms/regular_elements.vue' /* webpackChunkName: "pages/forms/regular_elements" */))
const _e40608ae = () => interopDefault(import('../pages/forms/validation.vue' /* webpackChunkName: "pages/forms/validation" */))
const _51c97b9b = () => interopDefault(import('../pages/forms/wizard.vue' /* webpackChunkName: "pages/forms/wizard" */))
const _4e6a0a71 = () => interopDefault(import('../pages/forms/wizard/step1.vue' /* webpackChunkName: "pages/forms/wizard/step1" */))
const _4e7821f2 = () => interopDefault(import('../pages/forms/wizard/step2.vue' /* webpackChunkName: "pages/forms/wizard/step2" */))
const _4e863973 = () => interopDefault(import('../pages/forms/wizard/step3.vue' /* webpackChunkName: "pages/forms/wizard/step3" */))
const _44c94d7d = () => interopDefault(import('../pages/klausul-disclaimer/image_cropper.vue' /* webpackChunkName: "pages/klausul-disclaimer/image_cropper" */))
const _887306f0 = () => interopDefault(import('../pages/mapping/image_cropper.vue' /* webpackChunkName: "pages/mapping/image_cropper" */))
const _45fe835c = () => interopDefault(import('../pages/masterdata/account_type/index.vue' /* webpackChunkName: "pages/masterdata/account_type/index" */))
const _7e967069 = () => interopDefault(import('../pages/masterdata/article_type/index.vue' /* webpackChunkName: "pages/masterdata/article_type/index" */))
const _607335f1 = () => interopDefault(import('../pages/masterdata/city/index.vue' /* webpackChunkName: "pages/masterdata/city/index" */))
const _3061b286 = () => interopDefault(import('../pages/masterdata/content_type/index.vue' /* webpackChunkName: "pages/masterdata/content_type/index" */))
const _5b58d245 = () => interopDefault(import('../pages/masterdata/hero_level/index.vue' /* webpackChunkName: "pages/masterdata/hero_level/index" */))
const _78a209e3 = () => interopDefault(import('../pages/masterdata/midtrans.vue' /* webpackChunkName: "pages/masterdata/midtrans" */))
const _468b87f6 = () => interopDefault(import('../pages/masterdata/province/index.vue' /* webpackChunkName: "pages/masterdata/province/index" */))
const _64505f1d = () => interopDefault(import('../pages/organization/image_cropper.vue' /* webpackChunkName: "pages/organization/image_cropper" */))
const _d2bd19ae = () => interopDefault(import('../pages/pages/blank.vue' /* webpackChunkName: "pages/pages/blank" */))
const _fb4589d6 = () => interopDefault(import('../pages/pages/blank_header_expanded.vue' /* webpackChunkName: "pages/pages/blank_header_expanded" */))
const _36454513 = () => interopDefault(import('../pages/pages/chat.vue' /* webpackChunkName: "pages/pages/chat" */))
const _db09f2a2 = () => interopDefault(import('../pages/pages/chat2.vue' /* webpackChunkName: "pages/pages/chat2" */))
const _419ba4d0 = () => interopDefault(import('../pages/pages/contact_list.vue' /* webpackChunkName: "pages/pages/contact_list" */))
const _b7536502 = () => interopDefault(import('../pages/pages/contact_list_single.vue' /* webpackChunkName: "pages/pages/contact_list_single" */))
const _b9846232 = () => interopDefault(import('../pages/pages/gallery.vue' /* webpackChunkName: "pages/pages/gallery" */))
const _615b8953 = () => interopDefault(import('../pages/pages/help_faq.vue' /* webpackChunkName: "pages/pages/help_faq" */))
const _12ba1421 = () => interopDefault(import('../pages/pages/invoices.vue' /* webpackChunkName: "pages/pages/invoices" */))
const _fd558438 = () => interopDefault(import('../pages/pages/invoices/index.vue' /* webpackChunkName: "pages/pages/invoices/index" */))
const _57bd2d68 = () => interopDefault(import('../pages/pages/invoices/_id.vue' /* webpackChunkName: "pages/pages/invoices/_id" */))
const _9a053296 = () => interopDefault(import('../pages/pages/issues.vue' /* webpackChunkName: "pages/pages/issues" */))
const _47195768 = () => interopDefault(import('../pages/pages/issues/details.vue' /* webpackChunkName: "pages/pages/issues/details" */))
const _750fb113 = () => interopDefault(import('../pages/pages/issues/details/_id.vue' /* webpackChunkName: "pages/pages/issues/details/_id" */))
const _0f5a9b68 = () => interopDefault(import('../pages/pages/issues/list.vue' /* webpackChunkName: "pages/pages/issues/list" */))
const _8025416e = () => interopDefault(import('../pages/pages/mailbox.vue' /* webpackChunkName: "pages/pages/mailbox" */))
const _18c4e9e8 = () => interopDefault(import('../pages/pages/mailbox/index.vue' /* webpackChunkName: "pages/pages/mailbox/index" */))
const _3effd34c = () => interopDefault(import('../pages/pages/mailbox/compose.vue' /* webpackChunkName: "pages/pages/mailbox/compose" */))
const _bc2c3728 = () => interopDefault(import('../pages/pages/mailbox/message/_id.vue' /* webpackChunkName: "pages/pages/mailbox/message/_id" */))
const _c0b49bd4 = () => interopDefault(import('../pages/pages/notes.vue' /* webpackChunkName: "pages/pages/notes" */))
const _21dad424 = () => interopDefault(import('../pages/pages/poi_listing.vue' /* webpackChunkName: "pages/pages/poi_listing" */))
const _24748ff9 = () => interopDefault(import('../pages/pages/pricing_tables.vue' /* webpackChunkName: "pages/pages/pricing_tables" */))
const _69b1c6de = () => interopDefault(import('../pages/pages/settings.vue' /* webpackChunkName: "pages/pages/settings" */))
const _2422d127 = () => interopDefault(import('../pages/pages/task_board.vue' /* webpackChunkName: "pages/pages/task_board" */))
const _961e6d20 = () => interopDefault(import('../pages/pages/user_profile.vue' /* webpackChunkName: "pages/pages/user_profile" */))
const _a7583d02 = () => interopDefault(import('../pages/plugins/ajax.vue' /* webpackChunkName: "pages/plugins/ajax" */))
const _24adb01d = () => interopDefault(import('../pages/plugins/calendar.vue' /* webpackChunkName: "pages/plugins/calendar" */))
const _3ea1c918 = () => interopDefault(import('../pages/plugins/charts.vue' /* webpackChunkName: "pages/plugins/charts" */))
const _1c5e6720 = () => interopDefault(import('../pages/plugins/code_editor.vue' /* webpackChunkName: "pages/plugins/code_editor" */))
const _33f2c36c = () => interopDefault(import('../pages/plugins/data_grid.vue' /* webpackChunkName: "pages/plugins/data_grid" */))
const _00d1b52e = () => interopDefault(import('../pages/plugins/datatables.vue' /* webpackChunkName: "pages/plugins/datatables" */))
const _76507a7a = () => interopDefault(import('../pages/plugins/diff_tool.vue' /* webpackChunkName: "pages/plugins/diff_tool" */))
const _5cad1aa4 = () => interopDefault(import('../pages/plugins/gantt_chart.vue' /* webpackChunkName: "pages/plugins/gantt_chart" */))
const _06bb2e4e = () => interopDefault(import('../pages/plugins/google_maps.vue' /* webpackChunkName: "pages/plugins/google_maps" */))
const _f27a8556 = () => interopDefault(import('../pages/plugins/idle_timeout.vue' /* webpackChunkName: "pages/plugins/idle_timeout" */))
const _78a98ada = () => interopDefault(import('../pages/plugins/image_cropper.vue' /* webpackChunkName: "pages/plugins/image_cropper" */))
const _c6c10c70 = () => interopDefault(import('../pages/plugins/image_cropper2.vue' /* webpackChunkName: "pages/plugins/image_cropper2" */))
const _1fbf03bc = () => interopDefault(import('../pages/plugins/push_notifications.vue' /* webpackChunkName: "pages/plugins/push_notifications" */))
const _e7548492 = () => interopDefault(import('../pages/plugins/tour.vue' /* webpackChunkName: "pages/plugins/tour" */))
const _0f03b3fd = () => interopDefault(import('../pages/plugins/tree.vue' /* webpackChunkName: "pages/plugins/tree" */))
const _579312f8 = () => interopDefault(import('../pages/plugins/vector_maps.vue' /* webpackChunkName: "pages/plugins/vector_maps" */))
const _08a8f244 = () => interopDefault(import('../pages/plugins/vue_good_table.vue' /* webpackChunkName: "pages/plugins/vue_good_table" */))
const _7b4b3b92 = () => interopDefault(import('../pages/report/individual/index.vue' /* webpackChunkName: "pages/report/individual/index" */))
const _393d1f0a = () => interopDefault(import('../pages/transaction/direct.vue' /* webpackChunkName: "pages/transaction/direct" */))
const _0db5cac1 = () => interopDefault(import('../pages/transaction/midtrans.vue' /* webpackChunkName: "pages/transaction/midtrans" */))
const _6e0a9e86 = () => interopDefault(import('../pages/transaction/order.vue' /* webpackChunkName: "pages/transaction/order" */))
const _10f6c65e = () => interopDefault(import('../pages/users/technician/index.vue' /* webpackChunkName: "pages/users/technician/index" */))
const _3d4c4fac = () => interopDefault(import('../pages/forms/advanced_elements/checkbox_radio.vue' /* webpackChunkName: "pages/forms/advanced_elements/checkbox_radio" */))
const _78f38fd5 = () => interopDefault(import('../pages/forms/advanced_elements/color_picker.vue' /* webpackChunkName: "pages/forms/advanced_elements/color_picker" */))
const _6ed629a4 = () => interopDefault(import('../pages/forms/advanced_elements/date_picker.vue' /* webpackChunkName: "pages/forms/advanced_elements/date_picker" */))
const _30d87766 = () => interopDefault(import('../pages/forms/advanced_elements/date_range_picker.vue' /* webpackChunkName: "pages/forms/advanced_elements/date_range_picker" */))
const _2d9ef8db = () => interopDefault(import('../pages/forms/advanced_elements/inputmask.vue' /* webpackChunkName: "pages/forms/advanced_elements/inputmask" */))
const _01bb384c = () => interopDefault(import('../pages/forms/advanced_elements/multiselect.vue' /* webpackChunkName: "pages/forms/advanced_elements/multiselect" */))
const _48c2c88e = () => interopDefault(import('../pages/forms/advanced_elements/range_slider.vue' /* webpackChunkName: "pages/forms/advanced_elements/range_slider" */))
const _3456cd70 = () => interopDefault(import('../pages/forms/advanced_elements/rating.vue' /* webpackChunkName: "pages/forms/advanced_elements/rating" */))
const _6cfa0dca = () => interopDefault(import('../pages/forms/advanced_elements/select2.vue' /* webpackChunkName: "pages/forms/advanced_elements/select2" */))
const _dc022966 = () => interopDefault(import('../pages/forms/advanced_elements/switches.vue' /* webpackChunkName: "pages/forms/advanced_elements/switches" */))
const _d43425b6 = () => interopDefault(import('../pages/forms/advanced_elements/time_picker.vue' /* webpackChunkName: "pages/forms/advanced_elements/time_picker" */))
const _241e5eb6 = () => interopDefault(import('../pages/forms/examples/advertising_evaluation_form.vue' /* webpackChunkName: "pages/forms/examples/advertising_evaluation_form" */))
const _6a0a5770 = () => interopDefault(import('../pages/forms/examples/booking_form.vue' /* webpackChunkName: "pages/forms/examples/booking_form" */))
const _c22f8804 = () => interopDefault(import('../pages/forms/examples/car_rental_form.vue' /* webpackChunkName: "pages/forms/examples/car_rental_form" */))
const _370fb107 = () => interopDefault(import('../pages/forms/examples/checkout_form.vue' /* webpackChunkName: "pages/forms/examples/checkout_form" */))
const _ed2cf28c = () => interopDefault(import('../pages/forms/examples/contact_forms.vue' /* webpackChunkName: "pages/forms/examples/contact_forms" */))
const _5057599b = () => interopDefault(import('../pages/forms/examples/job_application_form.vue' /* webpackChunkName: "pages/forms/examples/job_application_form" */))
const _07fbdb83 = () => interopDefault(import('../pages/forms/examples/medical_history_form.vue' /* webpackChunkName: "pages/forms/examples/medical_history_form" */))
const _2c728df4 = () => interopDefault(import('../pages/forms/examples/registration_form.vue' /* webpackChunkName: "pages/forms/examples/registration_form" */))
const _8495fc10 = () => interopDefault(import('../pages/forms/examples/rental_application_form.vue' /* webpackChunkName: "pages/forms/examples/rental_application_form" */))
const _2528ff87 = () => interopDefault(import('../pages/forms/examples/transaction_feedback_form.vue' /* webpackChunkName: "pages/forms/examples/transaction_feedback_form" */))
const _186e4872 = () => interopDefault(import('../pages/forms/wysiwyg/ckeditor.vue' /* webpackChunkName: "pages/forms/wysiwyg/ckeditor" */))
const _3d09dcfa = () => interopDefault(import('../pages/forms/wysiwyg/quill.vue' /* webpackChunkName: "pages/forms/wysiwyg/quill" */))
const _878b7e06 = () => interopDefault(import('../pages/masterdata/account/admin/index.vue' /* webpackChunkName: "pages/masterdata/account/admin/index" */))
const _1254b20c = () => interopDefault(import('../pages/masterdata/account/technician/index.vue' /* webpackChunkName: "pages/masterdata/account/technician/index" */))
const _92090f14 = () => interopDefault(import('../pages/masterdata/account/users/index.vue' /* webpackChunkName: "pages/masterdata/account/users/index" */))
const _cb946ae4 = () => interopDefault(import('../pages/report/individual/image_cropper.vue' /* webpackChunkName: "pages/report/individual/image_cropper" */))
const _446bae50 = () => interopDefault(import('../pages/masterdata/account/admin/details/_id.vue' /* webpackChunkName: "pages/masterdata/account/admin/details/_id" */))
const _720952a7 = () => interopDefault(import('../pages/masterdata/account/technician/details/_id.vue' /* webpackChunkName: "pages/masterdata/account/technician/details/_id" */))
const _7b582191 = () => interopDefault(import('../pages/masterdata/account/users/details/_id.vue' /* webpackChunkName: "pages/masterdata/account/users/details/_id" */))
const _651edb26 = () => interopDefault(import('../pages/masterdata/account_type/details/_id.vue' /* webpackChunkName: "pages/masterdata/account_type/details/_id" */))
const _01281744 = () => interopDefault(import('../pages/masterdata/article_type/details/_id.vue' /* webpackChunkName: "pages/masterdata/article_type/details/_id" */))
const _53ec96cc = () => interopDefault(import('../pages/masterdata/city/details/_id.vue' /* webpackChunkName: "pages/masterdata/city/details/_id" */))
const _480e8fa1 = () => interopDefault(import('../pages/masterdata/content_type/details/_id.vue' /* webpackChunkName: "pages/masterdata/content_type/details/_id" */))
const _76b8a420 = () => interopDefault(import('../pages/masterdata/hero_level/details/_id.vue' /* webpackChunkName: "pages/masterdata/hero_level/details/_id" */))
const _0f7e9111 = () => interopDefault(import('../pages/masterdata/province/details/_id.vue' /* webpackChunkName: "pages/masterdata/province/details/_id" */))
const _5140c795 = () => interopDefault(import('../pages/report/individual/details/_id/index.vue' /* webpackChunkName: "pages/report/individual/details/_id/index" */))
const _5d8b6179 = () => interopDefault(import('../pages/users/technician/details/_id.vue' /* webpackChunkName: "pages/users/technician/details/_id" */))
const _79e7ba45 = () => interopDefault(import('../pages/report/individual/details/_id/details.vue' /* webpackChunkName: "pages/report/individual/details/_id/details" */))
const _c98e3b60 = () => interopDefault(import('../pages/admin/details/_id/index.vue' /* webpackChunkName: "pages/admin/details/_id/index" */))
const _6ca7a7dd = () => interopDefault(import('../pages/banner/details/_id/index.vue' /* webpackChunkName: "pages/banner/details/_id/index" */))
const _6b053800 = () => interopDefault(import('../pages/content/details/_content_id/index.vue' /* webpackChunkName: "pages/content/details/_content_id/index" */))
const _738f67d2 = () => interopDefault(import('../pages/disaster/details/_id/index.vue' /* webpackChunkName: "pages/disaster/details/_id/index" */))
const _a12989f8 = () => interopDefault(import('../pages/klausul-disclaimer/details/_id/index.vue' /* webpackChunkName: "pages/klausul-disclaimer/details/_id/index" */))
const _125826e2 = () => interopDefault(import('../pages/mapping/details/_id/index.vue' /* webpackChunkName: "pages/mapping/details/_id/index" */))
const _6a97cd0f = () => interopDefault(import('../pages/order/details/_id/index.vue' /* webpackChunkName: "pages/order/details/_id/index" */))
const _e1893a3e = () => interopDefault(import('../pages/organization/details/_id.vue' /* webpackChunkName: "pages/organization/details/_id" */))
const _195e6bbd = () => interopDefault(import('../pages/ticket/details/_id/index.vue' /* webpackChunkName: "pages/ticket/details/_id/index" */))
const _552a7246 = () => interopDefault(import('../pages/users/details/_id.vue' /* webpackChunkName: "pages/users/details/_id" */))
const _5c4dc9a9 = () => interopDefault(import('../pages/version-control/details/_id/index.vue' /* webpackChunkName: "pages/version-control/details/_id/index" */))
const _26441240 = () => interopDefault(import('../pages/admin/details/_id/details.vue' /* webpackChunkName: "pages/admin/details/_id/details" */))
const _5717a88d = () => interopDefault(import('../pages/banner/details/_id/details.vue' /* webpackChunkName: "pages/banner/details/_id/details" */))
const _4740172a = () => interopDefault(import('../pages/content/details/_content_id/content_details.vue' /* webpackChunkName: "pages/content/details/_content_id/content_details" */))
const _1384fc7c = () => interopDefault(import('../pages/disaster/details/_id/content_details.vue' /* webpackChunkName: "pages/disaster/details/_id/content_details" */))
const _1179e418 = () => interopDefault(import('../pages/klausul-disclaimer/details/_id/details.vue' /* webpackChunkName: "pages/klausul-disclaimer/details/_id/details" */))
const _074588bf = () => interopDefault(import('../pages/mapping/details/_id/details.vue' /* webpackChunkName: "pages/mapping/details/_id/details" */))
const _ccd96d82 = () => interopDefault(import('../pages/order/details/_id/details.vue' /* webpackChunkName: "pages/order/details/_id/details" */))
const _9da61726 = () => interopDefault(import('../pages/ticket/details/_id/details.vue' /* webpackChunkName: "pages/ticket/details/_id/details" */))
const _1486f14e = () => interopDefault(import('../pages/version-control/details/_id/details.vue' /* webpackChunkName: "pages/version-control/details/_id/details" */))
const _1a784232 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/admin",
    component: _33707d72,
    name: "admin"
  }, {
    path: "/banner",
    component: _69cd2302,
    name: "banner"
  }, {
    path: "/content",
    component: _0c3a701c,
    name: "content"
  }, {
    path: "/dashboard",
    component: _49aaa312,
    name: "dashboard"
  }, {
    path: "/disaster",
    component: _0d51b5f4,
    name: "disaster"
  }, {
    path: "/klausul-disclaimer",
    component: _090977b4,
    name: "klausul-disclaimer"
  }, {
    path: "/login_page",
    component: _1b038a16,
    name: "login_page"
  }, {
    path: "/mapping",
    component: _3391ae31,
    name: "mapping"
  }, {
    path: "/order",
    component: _49c24eb1,
    name: "order"
  }, {
    path: "/organization",
    component: _417df5c6,
    name: "organization"
  }, {
    path: "/ticket",
    component: _1fef025f,
    name: "ticket"
  }, {
    path: "/users",
    component: _2e31b4eb,
    name: "users"
  }, {
    path: "/version-control",
    component: _7feae24b,
    name: "version-control"
  }, {
    path: "/banner/image_cropper",
    component: _5c680ed6,
    name: "banner-image_cropper"
  }, {
    path: "/components/accordion",
    component: _1bc75842,
    name: "components-accordion"
  }, {
    path: "/components/alert",
    component: _13f8c573,
    name: "components-alert"
  }, {
    path: "/components/animations",
    component: _b6fc0f30,
    name: "components-animations"
  }, {
    path: "/components/avatars",
    component: _edd5b8de,
    name: "components-avatars"
  }, {
    path: "/components/badge_label",
    component: _81800ba2,
    name: "components-badge_label"
  }, {
    path: "/components/base",
    component: _737de58a,
    name: "components-base"
  }, {
    path: "/components/breadcrumb",
    component: _0ce968fc,
    name: "components-breadcrumb"
  }, {
    path: "/components/buttons",
    component: _08887d58,
    name: "components-buttons"
  }, {
    path: "/components/cards",
    component: _5d93c7fa,
    name: "components-cards"
  }, {
    path: "/components/color_palette",
    component: _67d86594,
    name: "components-color_palette"
  }, {
    path: "/components/drop_dropdowns",
    component: _6bb9fa2b,
    name: "components-drop_dropdowns"
  }, {
    path: "/components/fab_buttons",
    component: _63051ca0,
    name: "components-fab_buttons"
  }, {
    path: "/components/filters",
    component: _3c5b94d2,
    name: "components-filters"
  }, {
    path: "/components/footer",
    component: _4ff97b14,
    name: "components-footer"
  }, {
    path: "/components/grid",
    component: _25e1b102,
    name: "components-grid"
  }, {
    path: "/components/height",
    component: _5422b0a0,
    name: "components-height"
  }, {
    path: "/components/icons",
    component: _194e8031,
    name: "components-icons"
  }, {
    path: "/components/lists",
    component: _2338db2c,
    name: "components-lists"
  }, {
    path: "/components/masonry",
    component: _e1928bc8,
    name: "components-masonry"
  }, {
    path: "/components/modals_dialogs",
    component: _dbd6ed6a,
    name: "components-modals_dialogs"
  }, {
    path: "/components/notifications",
    component: _f42d8802,
    name: "components-notifications"
  }, {
    path: "/components/pagination",
    component: _023fcd13,
    name: "components-pagination"
  }, {
    path: "/components/progress_spinners",
    component: _5cecbe3a,
    name: "components-progress_spinners"
  }, {
    path: "/components/scrollable",
    component: _c0c7d6c0,
    name: "components-scrollable"
  }, {
    path: "/components/slider",
    component: _3d3c2b5a,
    name: "components-slider"
  }, {
    path: "/components/sortable",
    component: _1985729e,
    name: "components-sortable"
  }, {
    path: "/components/tables",
    component: _baa6e244,
    name: "components-tables"
  }, {
    path: "/components/tabs",
    component: _b5d41892,
    name: "components-tabs"
  }, {
    path: "/components/timeline",
    component: _16f97b0c,
    name: "components-timeline"
  }, {
    path: "/components/toolbar",
    component: _46d2875c,
    name: "components-toolbar"
  }, {
    path: "/components/tooltips",
    component: _a8ec80ee,
    name: "components-tooltips"
  }, {
    path: "/components/transitions",
    component: _6a92c4d6,
    name: "components-transitions"
  }, {
    path: "/components/width",
    component: _681124dd,
    name: "components-width"
  }, {
    path: "/content/image_cropper",
    component: _1ef02373,
    name: "content-image_cropper"
  }, {
    path: "/dashboard/v1",
    component: _e3be7ab4,
    name: "dashboard-v1"
  }, {
    path: "/dashboard/v2",
    component: _e3a24bb2,
    name: "dashboard-v2"
  }, {
    path: "/disaster/image_cropper",
    component: _b69fdd6a,
    name: "disaster-image_cropper"
  }, {
    path: "/forms/dynamic_fields",
    component: _75c26de9,
    name: "forms-dynamic_fields"
  }, {
    path: "/forms/regular_elements",
    component: _430e5aea,
    name: "forms-regular_elements"
  }, {
    path: "/forms/validation",
    component: _e40608ae,
    name: "forms-validation"
  }, {
    path: "/forms/wizard",
    component: _51c97b9b,
    name: "forms-wizard",
    children: [{
      path: "step1",
      component: _4e6a0a71,
      name: "forms-wizard-step1"
    }, {
      path: "step2",
      component: _4e7821f2,
      name: "forms-wizard-step2"
    }, {
      path: "step3",
      component: _4e863973,
      name: "forms-wizard-step3"
    }]
  }, {
    path: "/klausul-disclaimer/image_cropper",
    component: _44c94d7d,
    name: "klausul-disclaimer-image_cropper"
  }, {
    path: "/mapping/image_cropper",
    component: _887306f0,
    name: "mapping-image_cropper"
  }, {
    path: "/masterdata/account_type",
    component: _45fe835c,
    name: "masterdata-account_type"
  }, {
    path: "/masterdata/article_type",
    component: _7e967069,
    name: "masterdata-article_type"
  }, {
    path: "/masterdata/city",
    component: _607335f1,
    name: "masterdata-city"
  }, {
    path: "/masterdata/content_type",
    component: _3061b286,
    name: "masterdata-content_type"
  }, {
    path: "/masterdata/hero_level",
    component: _5b58d245,
    name: "masterdata-hero_level"
  }, {
    path: "/masterdata/midtrans",
    component: _78a209e3,
    name: "masterdata-midtrans"
  }, {
    path: "/masterdata/province",
    component: _468b87f6,
    name: "masterdata-province"
  }, {
    path: "/organization/image_cropper",
    component: _64505f1d,
    name: "organization-image_cropper"
  }, {
    path: "/pages/blank",
    component: _d2bd19ae,
    name: "pages-blank"
  }, {
    path: "/pages/blank_header_expanded",
    component: _fb4589d6,
    name: "pages-blank_header_expanded"
  }, {
    path: "/pages/chat",
    component: _36454513,
    name: "pages-chat"
  }, {
    path: "/pages/chat2",
    component: _db09f2a2,
    name: "pages-chat2"
  }, {
    path: "/pages/contact_list",
    component: _419ba4d0,
    name: "pages-contact_list"
  }, {
    path: "/pages/contact_list_single",
    component: _b7536502,
    name: "pages-contact_list_single"
  }, {
    path: "/pages/gallery",
    component: _b9846232,
    name: "pages-gallery"
  }, {
    path: "/pages/help_faq",
    component: _615b8953,
    name: "pages-help_faq"
  }, {
    path: "/pages/invoices",
    component: _12ba1421,
    children: [{
      path: "",
      component: _fd558438,
      name: "pages-invoices"
    }, {
      path: ":id",
      component: _57bd2d68,
      name: "pages-invoices-id"
    }]
  }, {
    path: "/pages/issues",
    component: _9a053296,
    name: "pages-issues",
    children: [{
      path: "details",
      component: _47195768,
      name: "pages-issues-details",
      children: [{
        path: ":id?",
        component: _750fb113,
        name: "pages-issues-details-id"
      }]
    }, {
      path: "list",
      component: _0f5a9b68,
      name: "pages-issues-list"
    }]
  }, {
    path: "/pages/mailbox",
    component: _8025416e,
    children: [{
      path: "",
      component: _18c4e9e8,
      name: "pages-mailbox"
    }, {
      path: "compose",
      component: _3effd34c,
      name: "pages-mailbox-compose"
    }, {
      path: "message/:id?",
      component: _bc2c3728,
      name: "pages-mailbox-message-id"
    }]
  }, {
    path: "/pages/notes",
    component: _c0b49bd4,
    name: "pages-notes"
  }, {
    path: "/pages/poi_listing",
    component: _21dad424,
    name: "pages-poi_listing"
  }, {
    path: "/pages/pricing_tables",
    component: _24748ff9,
    name: "pages-pricing_tables"
  }, {
    path: "/pages/settings",
    component: _69b1c6de,
    name: "pages-settings"
  }, {
    path: "/pages/task_board",
    component: _2422d127,
    name: "pages-task_board"
  }, {
    path: "/pages/user_profile",
    component: _961e6d20,
    name: "pages-user_profile"
  }, {
    path: "/plugins/ajax",
    component: _a7583d02,
    name: "plugins-ajax"
  }, {
    path: "/plugins/calendar",
    component: _24adb01d,
    name: "plugins-calendar"
  }, {
    path: "/plugins/charts",
    component: _3ea1c918,
    name: "plugins-charts"
  }, {
    path: "/plugins/code_editor",
    component: _1c5e6720,
    name: "plugins-code_editor"
  }, {
    path: "/plugins/data_grid",
    component: _33f2c36c,
    name: "plugins-data_grid"
  }, {
    path: "/plugins/datatables",
    component: _00d1b52e,
    name: "plugins-datatables"
  }, {
    path: "/plugins/diff_tool",
    component: _76507a7a,
    name: "plugins-diff_tool"
  }, {
    path: "/plugins/gantt_chart",
    component: _5cad1aa4,
    name: "plugins-gantt_chart"
  }, {
    path: "/plugins/google_maps",
    component: _06bb2e4e,
    name: "plugins-google_maps"
  }, {
    path: "/plugins/idle_timeout",
    component: _f27a8556,
    name: "plugins-idle_timeout"
  }, {
    path: "/plugins/image_cropper",
    component: _78a98ada,
    name: "plugins-image_cropper"
  }, {
    path: "/plugins/image_cropper2",
    component: _c6c10c70,
    name: "plugins-image_cropper2"
  }, {
    path: "/plugins/push_notifications",
    component: _1fbf03bc,
    name: "plugins-push_notifications"
  }, {
    path: "/plugins/tour",
    component: _e7548492,
    name: "plugins-tour"
  }, {
    path: "/plugins/tree",
    component: _0f03b3fd,
    name: "plugins-tree"
  }, {
    path: "/plugins/vector_maps",
    component: _579312f8,
    name: "plugins-vector_maps"
  }, {
    path: "/plugins/vue_good_table",
    component: _08a8f244,
    name: "plugins-vue_good_table"
  }, {
    path: "/report/individual",
    component: _7b4b3b92,
    name: "report-individual"
  }, {
    path: "/transaction/direct",
    component: _393d1f0a,
    name: "transaction-direct"
  }, {
    path: "/transaction/midtrans",
    component: _0db5cac1,
    name: "transaction-midtrans"
  }, {
    path: "/transaction/order",
    component: _6e0a9e86,
    name: "transaction-order"
  }, {
    path: "/users/technician",
    component: _10f6c65e,
    name: "users-technician"
  }, {
    path: "/forms/advanced_elements/checkbox_radio",
    component: _3d4c4fac,
    name: "forms-advanced_elements-checkbox_radio"
  }, {
    path: "/forms/advanced_elements/color_picker",
    component: _78f38fd5,
    name: "forms-advanced_elements-color_picker"
  }, {
    path: "/forms/advanced_elements/date_picker",
    component: _6ed629a4,
    name: "forms-advanced_elements-date_picker"
  }, {
    path: "/forms/advanced_elements/date_range_picker",
    component: _30d87766,
    name: "forms-advanced_elements-date_range_picker"
  }, {
    path: "/forms/advanced_elements/inputmask",
    component: _2d9ef8db,
    name: "forms-advanced_elements-inputmask"
  }, {
    path: "/forms/advanced_elements/multiselect",
    component: _01bb384c,
    name: "forms-advanced_elements-multiselect"
  }, {
    path: "/forms/advanced_elements/range_slider",
    component: _48c2c88e,
    name: "forms-advanced_elements-range_slider"
  }, {
    path: "/forms/advanced_elements/rating",
    component: _3456cd70,
    name: "forms-advanced_elements-rating"
  }, {
    path: "/forms/advanced_elements/select2",
    component: _6cfa0dca,
    name: "forms-advanced_elements-select2"
  }, {
    path: "/forms/advanced_elements/switches",
    component: _dc022966,
    name: "forms-advanced_elements-switches"
  }, {
    path: "/forms/advanced_elements/time_picker",
    component: _d43425b6,
    name: "forms-advanced_elements-time_picker"
  }, {
    path: "/forms/examples/advertising_evaluation_form",
    component: _241e5eb6,
    name: "forms-examples-advertising_evaluation_form"
  }, {
    path: "/forms/examples/booking_form",
    component: _6a0a5770,
    name: "forms-examples-booking_form"
  }, {
    path: "/forms/examples/car_rental_form",
    component: _c22f8804,
    name: "forms-examples-car_rental_form"
  }, {
    path: "/forms/examples/checkout_form",
    component: _370fb107,
    name: "forms-examples-checkout_form"
  }, {
    path: "/forms/examples/contact_forms",
    component: _ed2cf28c,
    name: "forms-examples-contact_forms"
  }, {
    path: "/forms/examples/job_application_form",
    component: _5057599b,
    name: "forms-examples-job_application_form"
  }, {
    path: "/forms/examples/medical_history_form",
    component: _07fbdb83,
    name: "forms-examples-medical_history_form"
  }, {
    path: "/forms/examples/registration_form",
    component: _2c728df4,
    name: "forms-examples-registration_form"
  }, {
    path: "/forms/examples/rental_application_form",
    component: _8495fc10,
    name: "forms-examples-rental_application_form"
  }, {
    path: "/forms/examples/transaction_feedback_form",
    component: _2528ff87,
    name: "forms-examples-transaction_feedback_form"
  }, {
    path: "/forms/wysiwyg/ckeditor",
    component: _186e4872,
    name: "forms-wysiwyg-ckeditor"
  }, {
    path: "/forms/wysiwyg/quill",
    component: _3d09dcfa,
    name: "forms-wysiwyg-quill"
  }, {
    path: "/masterdata/account/admin",
    component: _878b7e06,
    name: "masterdata-account-admin"
  }, {
    path: "/masterdata/account/technician",
    component: _1254b20c,
    name: "masterdata-account-technician"
  }, {
    path: "/masterdata/account/users",
    component: _92090f14,
    name: "masterdata-account-users"
  }, {
    path: "/report/individual/image_cropper",
    component: _cb946ae4,
    name: "report-individual-image_cropper"
  }, {
    path: "/masterdata/account/admin/details/:id?",
    component: _446bae50,
    name: "masterdata-account-admin-details-id"
  }, {
    path: "/masterdata/account/technician/details/:id?",
    component: _720952a7,
    name: "masterdata-account-technician-details-id"
  }, {
    path: "/masterdata/account/users/details/:id?",
    component: _7b582191,
    name: "masterdata-account-users-details-id"
  }, {
    path: "/masterdata/account_type/details/:id?",
    component: _651edb26,
    name: "masterdata-account_type-details-id"
  }, {
    path: "/masterdata/article_type/details/:id?",
    component: _01281744,
    name: "masterdata-article_type-details-id"
  }, {
    path: "/masterdata/city/details/:id?",
    component: _53ec96cc,
    name: "masterdata-city-details-id"
  }, {
    path: "/masterdata/content_type/details/:id?",
    component: _480e8fa1,
    name: "masterdata-content_type-details-id"
  }, {
    path: "/masterdata/hero_level/details/:id?",
    component: _76b8a420,
    name: "masterdata-hero_level-details-id"
  }, {
    path: "/masterdata/province/details/:id?",
    component: _0f7e9111,
    name: "masterdata-province-details-id"
  }, {
    path: "/report/individual/details/:id?",
    component: _5140c795,
    name: "report-individual-details-id"
  }, {
    path: "/users/technician/details/:id?",
    component: _5d8b6179,
    name: "users-technician-details-id"
  }, {
    path: "/report/individual/details/:id?/details",
    component: _79e7ba45,
    name: "report-individual-details-id-details"
  }, {
    path: "/admin/details/:id?",
    component: _c98e3b60,
    name: "admin-details-id"
  }, {
    path: "/banner/details/:id?",
    component: _6ca7a7dd,
    name: "banner-details-id"
  }, {
    path: "/content/details/:content_id?",
    component: _6b053800,
    name: "content-details-content_id"
  }, {
    path: "/disaster/details/:id?",
    component: _738f67d2,
    name: "disaster-details-id"
  }, {
    path: "/klausul-disclaimer/details/:id",
    component: _a12989f8,
    name: "klausul-disclaimer-details-id"
  }, {
    path: "/mapping/details/:id?",
    component: _125826e2,
    name: "mapping-details-id"
  }, {
    path: "/order/details/:id?",
    component: _6a97cd0f,
    name: "order-details-id"
  }, {
    path: "/organization/details/:id?",
    component: _e1893a3e,
    name: "organization-details-id"
  }, {
    path: "/ticket/details/:id?",
    component: _195e6bbd,
    name: "ticket-details-id"
  }, {
    path: "/users/details/:id?",
    component: _552a7246,
    name: "users-details-id"
  }, {
    path: "/version-control/details/:id",
    component: _5c4dc9a9,
    name: "version-control-details-id"
  }, {
    path: "/admin/details/:id?/details",
    component: _26441240,
    name: "admin-details-id-details"
  }, {
    path: "/banner/details/:id?/details",
    component: _5717a88d,
    name: "banner-details-id-details"
  }, {
    path: "/content/details/:content_id?/content_details",
    component: _4740172a,
    name: "content-details-content_id-content_details"
  }, {
    path: "/disaster/details/:id?/content_details",
    component: _1384fc7c,
    name: "disaster-details-id-content_details"
  }, {
    path: "/klausul-disclaimer/details/:id/details",
    component: _1179e418,
    name: "klausul-disclaimer-details-id-details"
  }, {
    path: "/mapping/details/:id?/details",
    component: _074588bf,
    name: "mapping-details-id-details"
  }, {
    path: "/order/details/:id?/details",
    component: _ccd96d82,
    name: "order-details-id-details"
  }, {
    path: "/ticket/details/:id?/details",
    component: _9da61726,
    name: "ticket-details-id-details"
  }, {
    path: "/version-control/details/:id/details",
    component: _1486f14e,
    name: "version-control-details-id-details"
  }, {
    path: "/",
    component: _1a784232,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
