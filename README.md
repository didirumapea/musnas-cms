# Scutum Admin Template (Vue.js version)

## Quick Setup

``` bash
# install dependencies
$ npm sc-setup

# serve with hot reload at 192.168.1.188:3103
$ npm sc-dev

# serve with hot reload at localhost:3000
$ npm dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things works, check Scutum Admin documentation.
