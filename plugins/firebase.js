import firebase from 'firebase'
import 'firebase/auth'
// const config = {
// 	apiKey: "AIzaSyC_a_IoJPcLxh2U0m3-ws32cnmS34dPFwQ",
// 	authDomain: "bumi-kita.firebaseapp.com",
// 	databaseURL: "https://bumi-kita.firebaseio.com"
// };

if (!firebase.apps.length) {
	firebase.initializeApp({
		apiKey: "AIzaSyC_a_IoJPcLxh2U0m3-ws32cnmS34dPFwQ",
		authDomain: "bumi-kita.firebaseapp.com",
		databaseURL: "https://bumi-kita.firebaseio.com"
	})
}

// firebase.initializeApp(config);
const firedb = firebase.database()
// const storage = firebase.storage()

export { firedb }
// const rootRef = firebase.database().ref('/data');
// console.log(rootRef)
// DataSnapshot
// firedb.ref('cms/dashboard/monitoring').on("value", function(snapshot) {
//     const dataRef = snapshot.ref;
//     const dataKey = snapshot.key;
//     // console.log(dataRef)
//     console.log(snapshot.val())
// });
