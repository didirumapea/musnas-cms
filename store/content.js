// import config from '~/store/config'
import {showNotification} from '../plugins/global-function'
export const state = () => ({
	tempImageFile: null,
	dataContent: null,
	fileProgressPercentage: 0,
	statusContent: 'add'
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		console.log('server init')
	},
	//region CONTENT
	async getContent ({ commit }) {
		return await this.$axios.$get(`cms/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getArticleTypeByContentTypeId ({ commit }, payload) {
		return await this.$axios.$get(`cms/masterdata/article-type/list/content-type-id=${payload}/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListContent ({ commit }, payload) {
		return await this.$axios.$get(`cms/content/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
		// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListSearchContent ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/content/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addContent ({ commit }, payload) {
		let formData = new FormData()
		formData.append('content_type_id', payload.content_type_id)
		formData.append('disaster_id', payload.disaster_id)
		formData.append('article_type_id', payload.article_type_id)
		formData.append('title', payload.title)
		formData.append('content', payload.content)
		formData.append('publish_date', this.$moment(payload.publish_date).format())
		formData.append('video_url', payload.video_url)
		formData.append('image', payload.images)
		formData.append('file', payload.file)
		return await this.$axios.post('cms/content/add', formData,
			{
				headers: {
					'Content-Type': 'multipart/form-data',
					// 'Access-Control-Allow-Origin' : '*',
					// 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
				},
				onUploadProgress: function( progressEvent ) {
					let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
					commit('filePercentage', uploadPercentage)
				}.bind(this)
			})
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editContent ({ commit }, payload) {
		let formData = new FormData()
		formData.append('id', payload.id)
		formData.append('content_type_id', payload.content_type_id)
		formData.append('disaster_id', payload.disaster_id)
		formData.append('article_type_id', payload.article_type_id)
		formData.append('title', payload.title)
		formData.append('content', payload.content)
		formData.append('publish_date', this.$moment(payload.publish_date).format())
		formData.append('video_url', payload.video_url)
		formData.append('pdf_url', payload.pdf_url)
		payload.images !== null ? formData.append('image', payload.images) : '' // FILE
		payload.file !== undefined ? formData.append('file', payload.file) : '' // FILE
		return await this.$axios.post('cms/content/update', formData,
			{
				headers: {
					'Content-Type': 'multipart/form-data'
				},
				onUploadProgress: function( progressEvent ) {
					let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
					commit('filePercentage', uploadPercentage)
				}.bind(this)
			})
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveContent ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/content/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteContent ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('cms/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteContent ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/content/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion

	//region IS VERIFIED CONTENT
	async getVerifiedContent ({ commit }) {
		return await this.$axios.$get(`api/cms/content/verified/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getVerifiedListContent ({ commit }, payload) {
		return await this.$axios.$get(`cms/content/verified//list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getVerifiedListSearchContent ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/content/verified/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async updateIsVerifiedContent ({ commit }, payload) {
		return await this.$axios.post('cms/content/update-is-verified', payload)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	clearDataContent(state){
		state.tempImageFile = null
		state.dataContent = null
	},
	updateStatusContent(state, payload){
		state.statusContent = payload
	},
	filePercentage(state, payload){
		state.fileProgressPercentage = payload
	},
	savedStateAddContent(state, payload){
		// console.log(payload)
		state.dataContent = payload
	},
	setTempImageFileContent(state, payload){
		state.tempImageFile = payload
		state.dataContent.image_name = payload.name
		// console.log(state.tempImageFile)
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},
}


