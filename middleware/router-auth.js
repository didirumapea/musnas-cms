export default function ({ store, redirect, route }) {
  // console.log(store.commit('auth/checkUser'))
  store.commit('auth/checkUser')
	// console.log(route.name)

  if (store.state.auth.isLoggedIn && (route.name === 'login' || route.name === 'login-password' || route.name === 'daftar')) {
    // console.log(store.commit('auth/checkUser'))
    // console.log(store.commit)
    return redirect('/')
  }

  if (route.name === 'checkout') {
    store.commit('cart/checkItem')

    // console.log('checout route')
    // console.log(store.commit('auth/checkUser'))
    // console.log(store.commit)
    // return redirect('/')
  }
  // console.log(route.name)
  if (!store.state.auth.isLoggedIn && route.name === 'users') {
    return redirect('/login')
  }
  // if (isLogin(store) && route.name === 'login') {
  //   // console.log(store)
  //   return redirect('/admin')
  // }
//
//   if (!isLogin(store) && isAdminRoute(route)) {
//     // console.log(store)
//     return redirect('/login')
//   }
// }
//
// const isLogin = (store) => {
//   // console.log(store)
//   return (store && store.state && store.state.user)
// }
//
// const isAdminRoute = (route) => {
//   //   console.log(route)
//   if (route.matched.some(record => record.path === '/admin')) {
//     return true
//   }
}
