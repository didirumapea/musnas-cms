// import config from '~/store/config'
import {encryptAPI, decryptAPI, showNotification} from '../plugins/global-function'
export const state = () => ({
	tempImageFile: null,
	dataOrganization: null,
	fileProgressPercentage: 0,
	statusOrganization: 'add',
	listOrganization: null
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		console.log('server init')
	},
	//region ORGANIZATION
	async getListOrganization ({ commit }, payload) {
		// console.log(payload)
		let encodes = encryptAPI(JSON.stringify(payload.columnFilters))
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/organization/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}/column-filter=${encodes}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListOrganizationById ({ commit }, payload) {
		// console.log(payload)
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/organization/list/organization-id=${payload.id}/page=1/limit=10/column-sort=id/sort=desc`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getOrganization ({ commit }, payload) {
		payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/organization/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}/column-filter=null`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchOrganization ({ commit }, payload) {
		return await this.$axios.$get(`cms/organization/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addOrganization ({ commit }, payload) {
		console.log(payload)
		let formData = new FormData()
		formData.append('id_account_type', payload.id_account_type)
		formData.append('organization_code', payload.organization_code)
		formData.append('name', payload.name)
		formData.append('email', payload.email)
		formData.append('province_id', payload.province_id)
		formData.append('city_id', payload.city_id)
		formData.append('address', payload.address)
		formData.append('phone', payload.phone)
		formData.append('images', payload.images)
		return await this.$axios.post('cms/organization/add', formData,
			{
				headers: {
					'Content-Type': 'multipart/form-data',
					// 'Access-Control-Allow-Origin' : '*',
					// 'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
				},
				onUploadProgress: function( progressEvent ) {
					let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
					commit('filePercentage', uploadPercentage)
				}.bind(this)
			})
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveOrganization ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/organization/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editOrganization ({ commit }, payload) {
		// console.log(payload)
		let formData = new FormData()
		formData.append('id', payload.id)
		formData.append('id_account_type', payload.id_account_type)
		formData.append('organization_code', payload.organization_code)
		formData.append('name', payload.name)
		formData.append('email', payload.email)
		formData.append('province_id', payload.province_id)
		formData.append('city_id', payload.city_id)
		formData.append('address', payload.address)
		formData.append('phone', payload.phone)
		formData.append('img_url', payload.img_url)
		formData.append('images', payload.images)
		return await this.$axios.post('cms/organization/update', formData,
			{
				headers: {
					'Content-Type': 'multipart/form-data'
				},
				onUploadProgress: function( progressEvent ) {
					let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
					commit('filePercentage', uploadPercentage)
				}.bind(this)
			})
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteOrganization ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('cms/organization/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteOrganization ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/organization/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListOrganizationIdName ({ commit }, payload) {
		return await this.$axios.$get(`cms/organization/list/all/page=1/limit=100/column-sort=name/sort=asc`)
			.then((response) => {
				// console.log(response)
				commit('setListOrganization', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	setListOrganization(state, payload){
		state.listOrganization = payload.data
		// console.log(state.listOrganization)
	},
	clearDataOrganization(state){
		state.tempImageFile = null
		state.dataOrganization = null
	},
	updateStatusBanner(state, payload){
		state.statusOrganization = payload
	},
	filePercentage(state, payload){
		state.fileProgressPercentage = payload
	},
	savedStateAddOrganization(state, payload){
		// console.log(payload)
		state.dataOrganization = payload
	},
	setTempImageFile(state, payload){
		state.tempImageFile = payload
		state.dataOrganization.img_url = payload.name
		// console.log(state.tempImageFile)
	},
	setTempImageFileContent(state, payload){
		state.tempImageFile = payload
		state.dataOrganization.image = payload.name
		// console.log(state.tempImageFile)
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},

}


