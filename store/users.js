// import config from '~/store/config'
import {encryptAPI, decryptAPI, showNotification} from '../plugins/global-function'
export const state = () => ({
	tempImageFile: null,
	dataAddUsers: null,
	fileProgressPercentage: 0
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		console.log('server init')
	},
	//region USERS
	async getListUsers ({ commit }, payload) {
		// console.log(payload)
		let encodes = encryptAPI(JSON.stringify(payload.columnFilters))
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}/column-filter=${encodes}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListUsersById ({ commit }, payload) {
		// console.log(payload)
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/list/organization-id=${payload.id}/page=1/limit=10/column-sort=id/sort=desc`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getUsers ({ commit }, payload) {
		payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}/column-filter=null`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchUsers ({ commit }, payload) {
		return await this.$axios.$get(`cms/users/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addUsers ({ commit }, payload) {
		return await this.$axios.post('cms/users/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveUsers ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/users/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsVerifiedOrg ({ commit }, payload) {
		return await this.$axios.post('cms/users/update-is-verified-org', payload)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editUsers ({ commit }, payload) {
		// showNotification('good', false, 'success')

		return await this.$axios.post('cms/users/update', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteUsers ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('cms/users/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteUsers ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/users/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async sampelGetListUsers ({ commit }, payload) {
		// console.log(payload)
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/list/sampel/page=1/limit=100/column-sort=id/sort=desc`,
			{
				onDownloadProgress: (progressEvent) => {
					const totalLength = progressEvent.lengthComputable ? progressEvent.total : progressEvent.target.getResponseHeader('content-length') || progressEvent.target.getResponseHeader('x-decompressed-content-length');
					// console.log("onDownloadProgress", totalLength);
					if (totalLength !== null) {
						this.progressData = Math.round( (progressEvent.loaded * 100) / totalLength );
						console.log("onDownloadProgress", this.progressData + ' %');
					}
				},
			}

		)
			.then((response) => {
				console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async sampelGetListUsers2 ({ commit }, payload) {
		// console.log(payload)
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/list/sampel/page=1/limit=100/column-sort=id/sort=desc`,
			{
				onUploadProgress: (progressEvent) => {
					const totalLength = progressEvent.lengthComputable ? progressEvent.total : progressEvent.target.getResponseHeader('content-length') || progressEvent.target.getResponseHeader('x-decompressed-content-length');
					console.log("onUploadProgress", totalLength);
					if (totalLength !== null) {
						this.progressData = Math.round( (progressEvent.loaded * 100) / totalLength );
					}
				},
				onDownloadProgress: (progressEvent) => {
					const totalLength = progressEvent.lengthComputable ? progressEvent.total : progressEvent.target.getResponseHeader('content-length') || progressEvent.target.getResponseHeader('x-decompressed-content-length');
					console.log("onDownloadProgress", totalLength);
					if (totalLength !== null) {
						this.progressData = Math.round( (progressEvent.loaded * 100) / totalLength );
					}
				},
			}

		)
			.then((response) => {
				console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region USERS PRA REGISTER
	async getListUsersPreRegister ({ commit }, payload) {
		// console.log(payload)
		let encodes = encryptAPI(JSON.stringify(payload.columnFilters))
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/pre-register/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}/column-filter=${encodes}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListUsersPreRegisterById ({ commit }, payload) {
		// console.log(payload)
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/pre-register/list/organization-id=${payload.id}/page=1/limit=10/column-sort=id/sort=desc`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getUsersPreRegister ({ commit }, payload) {
		payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/pre-register/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}/column-filter=null`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchUsersPreRegister ({ commit }, payload) {
		return await this.$axios.$get(`cms/users/pre-register/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addUsersPreRegister ({ commit }, payload) {
		return await this.$axios.post('cms/users/pre-register/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveUsersPreRegister ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/users/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editUsersPreRegister ({ commit }, payload) {
		// showNotification('good', false, 'success')

		return await this.$axios.post('cms/users/update', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteUsersPreRegister ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('cms/users/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteUsersPreRegister ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/users/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async sampelGetListUsersPreRegister ({ commit }, payload) {
		// console.log(payload)
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/list/sampel/page=1/limit=100/column-sort=id/sort=desc`,
			{
				onDownloadProgress: (progressEvent) => {
					const totalLength = progressEvent.lengthComputable ? progressEvent.total : progressEvent.target.getResponseHeader('content-length') || progressEvent.target.getResponseHeader('x-decompressed-content-length');
					// console.log("onDownloadProgress", totalLength);
					if (totalLength !== null) {
						this.progressData = Math.round( (progressEvent.loaded * 100) / totalLength );
						console.log("onDownloadProgress", this.progressData + ' %');
					}
				},
			}

		)
			.then((response) => {
				console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async sampelGetListUsers2PreRegister ({ commit }, payload) {
		// console.log(payload)
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/list/sampel/page=1/limit=100/column-sort=id/sort=desc`,
			{
				onUploadProgress: (progressEvent) => {
					const totalLength = progressEvent.lengthComputable ? progressEvent.total : progressEvent.target.getResponseHeader('content-length') || progressEvent.target.getResponseHeader('x-decompressed-content-length');
					console.log("onUploadProgress", totalLength);
					if (totalLength !== null) {
						this.progressData = Math.round( (progressEvent.loaded * 100) / totalLength );
					}
				},
				onDownloadProgress: (progressEvent) => {
					const totalLength = progressEvent.lengthComputable ? progressEvent.total : progressEvent.target.getResponseHeader('content-length') || progressEvent.target.getResponseHeader('x-decompressed-content-length');
					console.log("onDownloadProgress", totalLength);
					if (totalLength !== null) {
						this.progressData = Math.round( (progressEvent.loaded * 100) / totalLength );
					}
				},
			}

		)
			.then((response) => {
				console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async downloadXlsxUserPreRegister ({ commit }, payload) {
		// console.log(payload)
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/pre-register/xlsx-create`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region USERS ADMIN
	async getListUsersAdmin ({ commit }, payload) {
		// console.log(payload)
		let encodes = encryptAPI(JSON.stringify(payload.columnFilters))
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/admin/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}/column-filter=${encodes}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListUsersAdminById ({ commit }, payload) {
		// console.log(payload)
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/pre-register/list/organization-id=${payload.id}/page=1/limit=10/column-sort=id/sort=desc`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getUsersAdmin ({ commit }, payload) {
		return await this.$axios.$get(`cms/users/admin/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchUsersAdmin ({ commit }, payload) {
		return await this.$axios.$get(`cms/users/admin/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addUsersAdmin ({ commit }, payload) {
		return await this.$axios.post('cms/users/admin/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveUsersAdmin ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/users/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsVerifiedAdmin ({ commit }, payload) {
		return await this.$axios.post('cms/users/admin/update-is-verified-admin', payload)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveSuperAdmin ({ commit }, payload) {
		return await this.$axios.post('cms/users/admin/update-is-super-admin', payload)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editUsersAdmin ({ commit }, payload) {
		// showNotification('good', false, 'success')

		return await this.$axios.post('cms/users/update', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteUsersAdmin ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('cms/users/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteUsersAdmin ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/users/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region USERS TECHNICIAN
	async getListUsersTechnician ({ commit }, payload) {
		// console.log(payload)
		let encodes = encryptAPI(JSON.stringify(payload.columnFilters))
		// payload.columnFilters = encryptAPI(JSON.stringify(payload.columnFilters))
		return await this.$axios.$get(`cms/users/admin/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}/column-filter=${encodes}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListUsersTechnicianById ({ commit }, payload) {
		return await this.$axios.$get(`cms/users/technician-user/list/id=${ payload }/page=1/limit=10/column-sort=id/sort=desc`)
			.then((response) => {
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getUsersTechnician ({ commit }, payload) {
		return await this.$axios.$get(`cms/users/admin/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchUsersTechnician ({ commit }, payload) {
		return await this.$axios.$get(`cms/users/admin/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addUsersTechnician ({ commit }, payload) {
		return await this.$axios.post('cms/users/technician-user/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveUsersTechnician ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/users/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsVerifiedTechnician ({ commit }, payload) {
		return await this.$axios.post('cms/users/admin/update-is-verified-admin', payload)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveSuperTechnician ({ commit }, payload) {
		return await this.$axios.post('cms/users/admin/update-is-super-admin', payload)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editUsersTechnician ({ commit }, payload) {
		// showNotification('good', false, 'success')

		return await this.$axios.post('cms/users/update', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteUsersTechnician ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('cms/users/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteUsersTechnician ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/users/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	filePercentage(state, payload){
		state.fileProgressPercentage = payload
	},
	savedStateAddUsers(state, payload){
		// console.log(payload)
		state.dataUsers = payload
	},
	setTempImageFile(state, payload){
		state.tempImageFile = payload
		state.dataAddUsers.image_url = payload.name
		// console.log(state.tempImageFile)
	},
	setTempImageFileContent(state, payload){
		state.tempImageFile = payload
		state.dataUsers.image = payload.name
		// console.log(state.tempImageFile)
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},

}


