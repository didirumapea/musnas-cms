import Vue from 'vue'
import moment from 'moment'
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

Vue.mixin({
  methods: {
  		timezoneOffset(){
  			return this.$moment().utcOffset()/60
		},
  	  modalPercentage(status){
  	  	const modalPercentage = UIkit.modal('#modal-center', { bgClose: false, escClose: true, modal: false, keyboard:false});
  	  	if (status === 'show'){
			modalPercentage.show()
		}else{
  	  		modalPercentage.hide()
		}
	  },
	  isVerifiedTypeLabel (priority) {
		  let priorityClass = null;
		  switch (priority) {
			  case 1:
				  priorityClass = 'uk-label-success';
				  break;
			  case 2:
				  priorityClass = 'uk-label-danger';
				  break;
			  default:
				  priorityClass = 'uk-label-primary';
				  break;
		  }
		  return priorityClass;
	  },
	  textStatus (priority) {
		  // console.log(priority)
		  let status = null;
		  switch (priority) {
			  case 1:
				  status = 'Verified';
				  break;
			  case 2:
				  status = 'Rejected';
				  break;
			  default:
				  status = 'Pending';
				  break;
		  }
		  return status;
	  },
	  isEmpty(str) {
	  	return (!str || 0 === str.length);
	  },
	  enc(plainText){
		  let b64 = this.$CryptoJS.AES.encrypt(plainText, process.env.SECRET_KEY).toString();
		  let e64 = this.$CryptoJS.enc.Base64.parse(b64);
		  // let eHex = e64.toString(this.$CryptoJS.enc.Hex);
		  return e64.toString(this.$CryptoJS.enc.Hex);
	  },
	  dec(cipherText){
		  let reb64 = this.$CryptoJS.enc.Hex.parse(cipherText);
		  let bytes = reb64.toString(this.$CryptoJS.enc.Base64);
		  let decrypt = this.$CryptoJS.AES.decrypt(bytes, process.env.SECRET_KEY);
		  // var plain = decrypt.toString(CryptoJS.enc.Utf8);
		  return decrypt.toString(this.$CryptoJS.enc.Utf8);
	  },
  	parseLocalTime(date){
  		// console.log(this.$store.state.setting.timezone)

  		return this.$moment(date).add(this.$store.state.setting.timezone, 'hours').format('YYYY-MM-DD HH:mm:ss')

	},
    shuffleArray(a){
      for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
      }
      return a;
    },
    convertIsoToDate(_date){
      // let date = new Date('2013-08-03T02:00:00Z');
      // let year = date.getFullYear();
      // let month = date.getMonth()+1;
      // let dt = date.getDate();
      // var str = '2011-04-11T10:20:30Z';
      let date = moment(_date);
      // let dateComponent = date.utc().format('DD MMMM YYYY');
      // let timeComponent = date.utc().format('HH:mm:ss');
      // console.log(dateComponent);
      // console.log(timeComponent);
      return date.utc().format('DD MMM YYYY');
    },
	showNotification (text, pos, status, persistent) {
		// text = '<div class="uk-flex uk-flex-middle"><i class="mdi mdi-check-all uk-margin-right md-color-light-green-400"></i>'+text+'</div>';
		text = '<div class="uk-flex uk-flex-middle">'+text+'</div>';
    	// let notification = {
		// 	default: 'My message',
		// 	persistent: 'My persistent message',
		// 	withIcon: '<div class="uk-flex uk-flex-middle"><i class="mdi mdi-check-all uk-margin-right md-color-light-green-400"></i>Message</div>',
		// 	long: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, labore laudantium eaque obcaecati',
		// 	withLink: '<div class="uk-flex uk-flex-middle"><span class="uk-flex-1">Message deleted!</span><a href="javascript:void(0)" class="md-color-amber-500 uk-margin-small-left">undo</a></div>'
		// }

	  let config = {};
	  config.timeout = persistent ? !persistent : 3000;
	  if(status) {
		  config.status = status;
	  }
	  if(pos) {
		  config.pos = pos;
	  }
	  UIkit.notification(text, config);
	},
    aCommonMethod () {
    	console.log('hellow from mixin')
        // return 'hello from mixin'
    },
    globsFunc(){
        return 'globs from mixins'
    },
    GzeroFill(val){
        let n = val
        let l = 7
        return ('0000000000'+n).slice(-l);
      },
    currencyFormat(n, currency){
        // WITH COMMA DIVIDER
        return currency + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
        });
    },
    currencyFormat2(n, currency){
      // WITH DOTS DIVIDER
      return currency + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
      });
    },
    htmlEntitiesConverter(htmlEntities){
        // console.log(entities.decode('&lt;&gt;&quot;&apos;&amp;&copy;&reg;&#8710;')); // <>"'&&copy;&reg;∆
        return entities.decode(htmlEntities)
    },
    groupBy(list, keyGetter) {
        const map = new Map();
        list.forEach((item) => {
             const key = keyGetter(item);
             const collection = map.get(key);
             if (!collection) {
                 map.set(key, [item]);
             } else {
                 collection.push(item);
             }
        });
        return map;
    },
  }
})

// FOR VUEX STORE
export function showNotification(text, pos, status, persistent) {
	text = '<div class="uk-flex uk-flex-middle"><i class="mdi mdi-check-all uk-margin-right md-color-light-green-400"></i>'+text+'</div>';
	// let notification = {
	// 	default: 'My message',
	// 	persistent: 'My persistent message',
	// 	withIcon: '<div class="uk-flex uk-flex-middle"><i class="mdi mdi-check-all uk-margin-right md-color-light-green-400"></i>Message</div>',
	// 	long: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, labore laudantium eaque obcaecati',
	// 	withLink: '<div class="uk-flex uk-flex-middle"><span class="uk-flex-1">Message deleted!</span><a href="javascript:void(0)" class="md-color-amber-500 uk-margin-small-left">undo</a></div>'
	// }

	let config = {};
	config.timeout = persistent ? !persistent : 3000;
	if(status) {
		config.status = status;
	}
	if(pos) {
		config.pos = pos;
	}
	UIkit.notification(text, config);
}

export function encryptAPI(plainText) {
	let b64 = Vue.CryptoJS.AES.encrypt(plainText, process.env.SECRET_KEY).toString();
	let e64 = Vue.CryptoJS.enc.Base64.parse(b64);
	// let eHex = e64.toString(this.$CryptoJS.enc.Hex);
	return e64.toString(Vue.CryptoJS.enc.Hex);
}
export function decryptAPI(cipherText) {
	let reb64 = Vue.CryptoJS.enc.Hex.parse(cipherText);
	let bytes = reb64.toString(Vue.CryptoJS.enc.Base64);
	let decrypt = Vue.CryptoJS.AES.decrypt(bytes, process.env.SECRET_KEY);
	// var plain = decrypt.toString(CryptoJS.enc.Utf8);
	return decrypt.toString(Vue.CryptoJS.enc.Utf8);
}
