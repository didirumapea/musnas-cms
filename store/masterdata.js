// import config from '~/store/config'
import {encryptAPI, decryptAPI, showNotification} from '../plugins/global-function'
export const state = () => ({
	tempImageFile: null,
	dataAccountType: null,
	dataContentType: null,
	dataArticleType: null,
	fileProgressPercentage: 0
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		console.log('server init')
	},
	//region ACCOUNT TYPE
	async getListAccountType ({ commit }, payload) {
		return await this.$axios.$get(`cms/masterdata/content-type/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListSearchAccountType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/masterdata/account-type/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addAccountType ({ commit }, payload) {
		return await this.$axios.post('cms/masterdata/account-type/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editAccountType ({ commit }, payload) {
		return await this.$axios.post('cms/masterdata/account-type/update', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveAccountType({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/masterdata/account-type/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteAccountType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteAccountType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/masterdata/account-type/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region ARTICLE TYPE
	async getListArticleType ({ commit }, payload) {
		return await this.$axios.$get(`cms/masterdata/article-type/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListSearchArticleType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/masterdata/article-type/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addArticleType ({ commit }, payload) {
		return await this.$axios.post('cms/masterdata/article-type/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editArticleType ({ commit }, payload) {
		return await this.$axios.post('cms/masterdata/article-type/update', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveArticleType({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/masterdata/article-type/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteArticleType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteArticleType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/masterdata/article-type/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region CONTENT TYPE
	async getListContentType ({ commit }, payload) {
		return await this.$axios.$get(`cms/masterdata/content-type/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListSearchContentType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/masterdata/content-type/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addContentType ({ commit }, payload) {
		return await this.$axios.post('cms/masterdata/content-type/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editContentType ({ commit }, payload) {
		return await this.$axios.post('cms/masterdata/content-type/update', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no sconnection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveContentType({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/masterdata/content-type/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteContentType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteContentType ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/masterdata/content-type/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region PROVINCE
	async getListProvince ({ commit }, payload) {
		return await this.$axios.$get(`cms/masterdata/province/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListSearchProvince ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/masterdata/province/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addProvince ({ commit }, payload) {
		return await this.$axios.post('cms/masterdata/content-type/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editProvince ({ commit }, payload) {
		return await this.$axios.post('cms/masterdata/province/update', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no sconnection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveProvince({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/masterdata/content-type/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteProvince ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteProvince ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/masterdata/province/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
	//region CITY
	async getListCity ({ commit }, payload) {
		return await this.$axios.$get(`cms/masterdata/city/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListSearchCity ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/masterdata/city/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addCity ({ commit }, payload) {
		return await this.$axios.post('cms/masterdata/content-type/add', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editCity ({ commit }, payload) {
		return await this.$axios.post('cms/masterdata/city/update', payload)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no sconnection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveCity({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/masterdata/content-type/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteCity ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteCity ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/masterdata/city/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	filePercentage(state, payload){
		state.fileProgressPercentage = payload
	},
	savedStateAddAccountType(state, payload){
		// console.log(payload)
		state.dataAccountType = payload
	},
	savedStateAddContentType(state, payload){
		// console.log(payload)
		state.dataContentType = payload
	},
	savedStateAddArticleType(state, payload){
		// console.log(payload)
		state.dataArticleType = payload
	},
	setTempImageFile(state, payload){
		state.tempImageFile = payload
		state.dataAddDisaster.image_url = payload.name
		// console.log(state.tempImageFile)
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},

}


