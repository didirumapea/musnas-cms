// import config from '~/store/config'
import {showNotification} from '../plugins/global-function'
export const state = () => ({
	tempImageFile: null,
	dataBanner: null,
	fileProgressPercentage: 0,
	statusBanner: 'add'
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		console.log('server init')
	},
	//region BANNER
	async getBanner ({ commit }) {
		return await this.$axios.$get(`api/cms/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListBanner ({ commit }, payload) {
		return await this.$axios.$get(`cms/banners/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
		// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListSearchBanner ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/banners/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addBanner ({ commit }, payload) {
		let formData = new FormData()
		formData.append('images', payload.images)
		formData.append('name', payload.name)
		formData.append('category', payload.category)
		formData.append('position', payload.position)
		formData.append('description', payload.description)
		formData.append('link_image', payload.link_image)
		formData.append('organization_id', payload.organization_id)
		return await this.$axios.post('cms/banners/add', formData,
			{
				headers: {
					'Content-Type': 'multipart/form-data'
				},
				onUploadProgress: function( progressEvent ) {
					let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
					this.commit('setting/filePercentage', uploadPercentage)
				}.bind(this)
			})
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editBanner ({ commit }, payload) {
		// console.log(payload)
		let formData = new FormData()
		formData.append('id', payload.id)
		formData.append('name', payload.name)
		formData.append('category', payload.category)
		formData.append('position', payload.position)
		formData.append('description', payload.description)
		formData.append('link_image', payload.link_image)
		formData.append('organization_id', payload.organization_id)
		formData.append('image_url', payload.image_url)
		formData.append('images', payload.images)
		return await this.$axios.post('cms/banners/update', formData,
			{
				headers: {
					'Content-Type': 'multipart/form-data'
				},
				onUploadProgress: function( progressEvent ) {
					let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
					this.commit('setting/filePercentage', uploadPercentage)
				}.bind(this)
			})
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveBanner ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/banners/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteBanner ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('cms/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteBanner ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/banners/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	clearData(state){
		state.tempImageFile = null
		state.dataContent = null
	},
	updateStatusBanner(state, payload){
		state.statusBanner = payload
	},
	filePercentage(state, payload){
		state.fileProgressPercentage = payload
	},
	savedStateAddBanner(state, payload){
		// console.log(payload)
		state.dataBanner = payload
	},
	setTempImageFile(state, payload){
		state.tempImageFile = payload
		state.dataBanner.image_url = payload.name
		// console.log(state.tempImageFile)
	},
	setTempImageFileBanner(state, payload){
		state.tempImageFile = payload
		state.dataContent.image = payload.name
		// console.log(state.tempImageFile)
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},
}


