// import config from '~/store/config'
import {showNotification} from '../plugins/global-function'
export const state = () => ({
	tempImageFile: null,
	dataContent: null,
	fileProgressPercentage: 0,
	statusContent: 'add'
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		console.log('server init')
	},
	//region REPORT INDIVIDUAL
	async getReportIndividual ({ commit }) {
		return await this.$axios.$get(`api/cms/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListReportIndividual ({ commit }, payload) {
		return await this.$axios.$get(`cms/report/individual/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
		// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListSearchReportIndividual ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/report/individual/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addReportIndividual ({ commit }, payload) {

		// console.log(this.state.auth.user)
		let formData = new FormData()
		formData.append('image', payload.images)
		formData.append('file', payload.file)
		formData.append('content_type_id', payload.content_type_id)
		formData.append('disaster_id', payload.disaster_id)
		formData.append('article_type_id', payload.article_type_id)
		formData.append('title', payload.title)
		formData.append('content', payload.content)
		formData.append('publish_date', payload.publish_date)
		return await this.$axios.post('cms/report/individual/add', formData,
			{
				headers: {
					'Content-Type': 'multipart/form-data'
				},
				onUploadProgress: function( progressEvent ) {
					let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
					commit('filePercentage', uploadPercentage)
				}.bind(this)
			})
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editReportIndividual ({ commit }, payload) {

		// console.log(payload)
		let formData = new FormData()
		formData.append('id', payload.id)
		formData.append('file', payload.file)
		formData.append('content_type_id', payload.content_type_id)
		formData.append('disaster_id', payload.disaster_id)
		formData.append('article_type_id', payload.article_type_id)
		formData.append('title', payload.title)
		formData.append('content', payload.content)
		formData.append('publish_date', payload.publish_date)
		formData.append('image', payload.images)
		return await this.$axios.post('cms/content/update', formData,
			{
				headers: {
					'Content-Type': 'multipart/form-data'
				},
				onUploadProgress: function( progressEvent ) {
					let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
					commit('filePercentage', uploadPercentage)
				}.bind(this)
			})
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveReportIndividual ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/report/individual/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteReportIndividual ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('cms/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteReportIndividual ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/report/individual/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async updateIsVerifiedReportIndividual ({ commit }, payload) {
		return await this.$axios.post('cms/report/individual/update-is-verified', payload)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion

}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	updateStatusContent(state, payload){
		state.statusContent = payload
	},
	filePercentage(state, payload){
		state.fileProgressPercentage = payload
	},
	savedStateAddContent(state, payload){
		// console.log(payload)
		state.dataContent = payload
	},
	setTempImageFile(state, payload){
		state.tempImageFile = payload
		state.dataAddDisaster.image_url = payload.name
		// console.log(state.tempImageFile)
	},
	setTempImageFileContent(state, payload){
		state.tempImageFile = payload
		state.dataContent.image = payload.name
		// console.log(state.tempImageFile)
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},
}


