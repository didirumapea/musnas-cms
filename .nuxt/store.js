import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const VUEX_PROPERTIES = ['state', 'getters', 'actions', 'mutations']

let store = {}

void (function updateModules () {
  store = normalizeRoot(require('../store/index.js'), 'store/index.js')

  // If store is an exported method = classic mode (deprecated)

  if (typeof store === 'function') {
    return console.warn('Classic mode for store/ is deprecated and will be removed in Nuxt 3.')
  }

  // Enforce store modules
  store.modules = store.modules || {}

  resolveStoreModules(require('../store/auth.js'), 'auth.js')
  resolveStoreModules(require('../store/banner.js'), 'banner.js')
  resolveStoreModules(require('../store/content.js'), 'content.js')
  resolveStoreModules(require('../store/dashboard.js'), 'dashboard.js')
  resolveStoreModules(require('../store/disaster.js'), 'disaster.js')
  resolveStoreModules(require('../store/examples.js'), 'examples.js')
  resolveStoreModules(require('../store/helper.js'), 'helper.js')
  resolveStoreModules(require('../store/issues.js'), 'issues.js')
  resolveStoreModules(require('../store/klausul_disclaimer.js'), 'klausul_disclaimer.js')
  resolveStoreModules(require('../store/mailbox.js'), 'mailbox.js')
  resolveStoreModules(require('../store/mapping.js'), 'mapping.js')
  resolveStoreModules(require('../store/masterdata.js'), 'masterdata.js')
  resolveStoreModules(require('../store/order.js'), 'order.js')
  resolveStoreModules(require('../store/organization.js'), 'organization.js')
  resolveStoreModules(require('../store/report.js'), 'report.js')
  resolveStoreModules(require('../store/setting.js'), 'setting.js')
  resolveStoreModules(require('../store/task_board.js'), 'task_board.js')
  resolveStoreModules(require('../store/ticket.js'), 'ticket.js')
  resolveStoreModules(require('../store/transaction.js'), 'transaction.js')
  resolveStoreModules(require('../store/users.js'), 'users.js')
  resolveStoreModules(require('../store/version_control.js'), 'version_control.js')

  // If the environment supports hot reloading...

  if (process.client && module.hot) {
    // Whenever any Vuex module is updated...
    module.hot.accept([
      '../store/auth.js',
      '../store/banner.js',
      '../store/content.js',
      '../store/dashboard.js',
      '../store/disaster.js',
      '../store/examples.js',
      '../store/helper.js',
      '../store/index.js',
      '../store/issues.js',
      '../store/klausul_disclaimer.js',
      '../store/mailbox.js',
      '../store/mapping.js',
      '../store/masterdata.js',
      '../store/order.js',
      '../store/organization.js',
      '../store/report.js',
      '../store/setting.js',
      '../store/task_board.js',
      '../store/ticket.js',
      '../store/transaction.js',
      '../store/users.js',
      '../store/version_control.js',
    ], () => {
      // Update `root.modules` with the latest definitions.
      updateModules()
      // Trigger a hot update in the store.
      window.$nuxt.$store.hotUpdate(store)
    })
  }
})()

// createStore
export const createStore = store instanceof Function ? store : () => {
  return new Vuex.Store(Object.assign({
    strict: (process.env.NODE_ENV !== 'production')
  }, store))
}

function normalizeRoot (moduleData, filePath) {
  moduleData = moduleData.default || moduleData

  if (moduleData.commit) {
    throw new Error(`[nuxt] ${filePath} should export a method that returns a Vuex instance.`)
  }

  if (typeof moduleData !== 'function') {
    // Avoid TypeError: setting a property that has only a getter when overwriting top level keys
    moduleData = Object.assign({}, moduleData)
  }
  return normalizeModule(moduleData, filePath)
}

function normalizeModule (moduleData, filePath) {
  if (moduleData.state && typeof moduleData.state !== 'function') {
    console.warn(`'state' should be a method that returns an object in ${filePath}`)

    const state = Object.assign({}, moduleData.state)
    // Avoid TypeError: setting a property that has only a getter when overwriting top level keys
    moduleData = Object.assign({}, moduleData, { state: () => state })
  }
  return moduleData
}

function resolveStoreModules (moduleData, filename) {
  moduleData = moduleData.default || moduleData
  // Remove store src + extension (./foo/index.js -> foo/index)
  const namespace = filename.replace(/\.(js|mjs)$/, '')
  const namespaces = namespace.split('/')
  let moduleName = namespaces[namespaces.length - 1]
  const filePath = `store/${filename}`

  moduleData = moduleName === 'state'
    ? normalizeState(moduleData, filePath)
    : normalizeModule(moduleData, filePath)

  // If src is a known Vuex property
  if (VUEX_PROPERTIES.includes(moduleName)) {
    const property = moduleName
    const storeModule = getStoreModule(store, namespaces, { isProperty: true })

    // Replace state since it's a function
    mergeProperty(storeModule, moduleData, property)
    return
  }

  // If file is foo/index.js, it should be saved as foo
  const isIndexModule = (moduleName === 'index')
  if (isIndexModule) {
    namespaces.pop()
    moduleName = namespaces[namespaces.length - 1]
  }

  const storeModule = getStoreModule(store, namespaces)

  for (const property of VUEX_PROPERTIES) {
    mergeProperty(storeModule, moduleData[property], property)
  }

  if (moduleData.namespaced === false) {
    delete storeModule.namespaced
  }
}

function normalizeState (moduleData, filePath) {
  if (typeof moduleData !== 'function') {
    console.warn(`${filePath} should export a method that returns an object`)
    const state = Object.assign({}, moduleData)
    return () => state
  }
  return normalizeModule(moduleData, filePath)
}

function getStoreModule (storeModule, namespaces, { isProperty = false } = {}) {
  // If ./mutations.js
  if (!namespaces.length || (isProperty && namespaces.length === 1)) {
    return storeModule
  }

  const namespace = namespaces.shift()

  storeModule.modules[namespace] = storeModule.modules[namespace] || {}
  storeModule.modules[namespace].namespaced = true
  storeModule.modules[namespace].modules = storeModule.modules[namespace].modules || {}

  return getStoreModule(storeModule.modules[namespace], namespaces, { isProperty })
}

function mergeProperty (storeModule, moduleData, property) {
  if (!moduleData) {
    return
  }

  if (property === 'state') {
    storeModule.state = moduleData || storeModule.state
  } else {
    storeModule[property] = Object.assign({}, storeModule[property], moduleData)
  }
}
