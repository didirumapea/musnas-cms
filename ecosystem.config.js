module.exports = {
  apps : [
    {
      name: "musnas-cms-dev",
      script: "npm",
      args: "run dev"
    },
    {
      name: "musnas-cms-prod",
      script: "npm",
      args: "run start"
    },
    {
      name: "musnas-cms-staging",
      script: "npm",
      args: "run start"
    }
  ]
}
