let urlCdn = 'https://cdn.museumnasional.or.id/';
if (process.env.NODE_ENV === 'development'){
	urlCdn = 'http://localhost:2121/musnas-files/';
	// urlCdn = 'https://cdnx.bumikita.or.id/';
}

import moment from "moment";

// STATE AS VARIABLE
export const state = () => ({
	pathFiles: `${urlCdn}assets/files/`,
    pathImagesUsers: urlCdn + 'assets/images/users/',
	pathImagesDisaster: urlCdn + 'assets/images/disaster/',
	pathImagesBanner: urlCdn + 'assets/images/banner/',
    pathImagesOrganization: urlCdn + 'assets/images/organization/',
    pathImagesContent: urlCdn + 'assets/images/content/',
    pathImagesReportIndividual: urlCdn + 'assets/images/report/individual/',
    d2: null,
    timezone: moment().utcOffset()/60,
	fileProgressPercentage: 0,
	statusFileProgress: 'add',
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
  async getFeatured1 ({ commit }) {
    return await this.$axios.$get('home/product/type=1')
      .then((response) => {
        return response
      })
      .catch(err => {
        // alert(JSON.stringify(err.response))

        if (err.response === undefined){
          return 'no connection'
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
        //   return err.response
      })
  },
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
	resetPercentage(state){
		state.fileProgressPercentage = 0
	},
	updateStatusFileProgress(state, payload){
		state.statusFileProgress = payload
	},
	filePercentage(state, payload){
		state.fileProgressPercentage = payload
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  pathImgPromoMerchant: state => {
    return state.pathImgPromoMerchant
  },
  pathImgCategory: state => {
    return state.pathImgCategory
  },
  pathImgBanner: state => {
    return state.pathImgBanner
  },
  pathImgMerchant: state => {
    return state.pathImgMerchant
  },
  pathImgMemberImages: state => {
    return state.pathImgMemberImages
  },
}
