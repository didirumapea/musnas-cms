// import config from '~/store/config'

// STATE AS VARIABLE
import {showNotification} from "../plugins/global-function";

export const state = () => ({
	listDirectTransaction: [],
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		// console.log('server init')
	},
	async getListDirectTransfer ({ commit }, payload) {
		return await this.$axios.$get(`transaction/direct/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListMidtrans ({ commit }, payload) {
		return await this.$axios.$get(`transaction/midtrans/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchDirectTransfer ({ commit }, payload) {
		console.log(payload)
		return await this.$axios.$get(`transaction/direct/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchMidtrans ({ commit }, payload) {
		return await this.$axios.$get(`transaction/midtrans/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//region ORDER
	async getListOrder ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`transaction/order/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async getListSearchOrder ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`transaction/order/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editOrder ({ commit }, payload) {
		console.log(payload)
		let data = {
			"id": payload.id,
			"title": payload.title,
			"image": payload.images,
			"content": payload.content,
			"type": payload.type
		}
		return await this.$axios.post('masterdata/content/update', data)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async manualUpdatePaymentStatus ({ commit }, payload) {
		console.log(payload)
		let data = {
			"id": payload.id,
			"no_order": payload.no_order,
			"trans_id": payload.trans_id,
			"payment_status": payload.payment_status === 'paid' ? 'pending' : 'paid',
		}
		return await this.$axios.post('transaction/order/update/payment-status', data)
			.then((response) => {
				// console.log(response)
				// showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteOrder ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteOrder ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('masterdata/sparepart/delete/'+payload)
			.then((response) => {
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
	setlistVoucherNotUsed(state, payload){
		// console.log(payload)
		state.listVoucherNotUsed = payload
		// this.$cookies.get('banner-top', payload)
	},
	setlistVoucherUsed(state, payload){
		// console.log(payload)
		state.listVoucherUsed = payload
		// this.$cookies.get('banner-top', payload)
	},
	setlistVoucherNotPay(state, payload){
		// console.log(payload)
		state.listVoucherNotPay = payload
		// this.$cookies.get('banner-top', payload)
	},
	useVoucher (state, id) {
		// console.log(state.listVoucherNotUsed.data, id)
		state.listVoucherNotUsed.data = state.listVoucherNotUsed.data.filter(item => item.id !== id)
	},
	voucherTimeExpired (state, id) {
		// console.log(state.listVoucherNotUsed.data, id)
		state.listVoucherNotPay.data = state.listVoucherNotPay.data.filter(item => item.id !== id)
	},
	// SAMPLE
	increment (state, payload) {
		state.count+=payload
	},
	add (state, text) {
		state.list.push({
			text: text,
			done: false
		})
	},
	remove (state, { todo }) {
		state.list.splice(state.list.indexOf(todo), 1)
	},
	toggle (state, todo) {
		todo.done = !todo.done
	}
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},

}
