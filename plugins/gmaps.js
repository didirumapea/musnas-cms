import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
	load: {
		key: 'AIzaSyA5eOIrjCQIpf8bivwzkHAjmMRUCG9NNlA',
		libraries: 'places'
	}
});
