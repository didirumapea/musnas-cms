// import config from '~/store/config'
import {showNotification} from '../plugins/global-function'
// import createPersistedState from "vuex-persistedstate";
// import SecureLS from "secure-ls";
// var ls = new SecureLS({ isCompression: false });
const moment = require('moment')
// STATE AS VARIABLE
export const state = () => ({
	user: null,
	isLoggedIn: false,
	count: 0,
	userToken: null
})
// export default ({store}) => {
// 	createPersistedState({
// 		storage: {
// 			getItem: key => ls.get(key),
// 			setItem: (key, value) => ls.set(key, value),
// 			removeItem: key => ls.remove(key)
// 		}
// 	})(store)
// }
// ACTIONS AS METHODS
export const actions = { // asyncronous

	// REGION ADMIN
  async login ({ commit }, payload) {
  		// console.log(payload)
	  let data = {
		  email: payload.email,
		  password: payload.password,
		  provider: ''
	  }
    return await this.$axios.post('cms/auth/admin/login', data)
      .then((response) => {
		  // console.log(response.data)
		if (response.data.success){
			showNotification(response.data.message, false, 'success')
			commit('setUser', response.data.data)
		}else{
			showNotification(response.data.message, false, 'danger')
		}
		  return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },

}
// MUTATION AS LOGIC
export const mutations = {
	// syncronous

  setUser(state, payload){
	  // console.log(payload)
	  // let dt = new Date(moment().add(1, 'd').format())
	  state.isLoggedIn = true;
	  state.user = payload
	  state.userToken = payload.token
	  // this.$cookies.set('session-user', payload, {
		//   path: '/',
		//   expires: dt
	  // })
	  // console.log(this.$cookies.get('session-user'))
  },
  checkUser(state, payload){
	  if (this.$cookies.get('session-user') === undefined || this.$cookies.get('session-user') === ''){
		  //console.log('its undefined')
		  state.isLoggedIn = false
	  }else{
		  state.user = this.$cookies.get('session-user')
		  state.isLoggedIn = true
	  }
	  // console.log(this.$cookies.get('session-ussr'))
  },
  logoutUser(state){
		showNotification('Logout Successfully', false, 'danger')
		// this.$toast.success('Logout Successfully')
		this.$cookies.removeAll()
		state.user = null
		state.isLoggedIn = false
	},

  increment (state, payload) {
    state.count+=payload
  },
  add (state, text) {
    state.list.push({
      text: text,
      done: false
    })
  },
  remove (state, { todo }) {
    state.list.splice(state.list.indexOf(todo), 1)
  },
  toggle (state, todo) {
    todo.done = !todo.done
  }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  savedQuestion: state => {
    return state.savedQuestion
  },

}
