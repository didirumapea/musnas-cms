export default function ({store, $axios, redirect }) {
	// SET GLOBAL HEADER
	$axios.defaults.headers.common = {
		"On-Project": "museum-nasional-indonesia-2k20",
		"Signature-Key": "4d6b0b7d92af49187c6a103c4044b03c79f48c21a36e9e617eedbad5bc5d48be",
	};
    $axios.onRequest(config => {
		if (store.state.auth.user !== null){
			// ADD NEW HEADER
			// console.log(store.state.auth.user.token)
			config.headers.common["Authorization"] = 'Bearer '+store.state.auth.user.token
		}
      if (process.env.NODE_ENV === 'development'){
        console.log('SPY: ' + config.url)
        // console.log( process.env.BASE_URL)
      }
    })
  }
