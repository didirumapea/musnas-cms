// import config from '~/store/config'
import {showNotification} from '../plugins/global-function'
export const state = () => ({
	tempImageFile: null,
	dataVersionControl: null,
	fileProgressPercentage: 0,
	statusVersionControl: 'add'
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		console.log('server init')
	},
	//region BANNER
	async getVersionControl ({ commit }) {
		return await this.$axios.$get(`api/cms/version-control/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListVersionControl ({ commit }, payload) {
		return await this.$axios.$get(`cms/banners/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
		// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListSearchVersionControl ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/banners/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addVersionControl ({ commit }, payload) {
		// console.log(multipart/form-data)
		// let formData = new FormData()
		// formData.append('version_code', payload.version_code)
		// formData.append('version_name', payload.version_name)
		return await this.$axios.post('cms/version-control/add', payload)
			// {
			// 	headers: {
			// 		'Content-Type': 'multipart/form-data'
			// 	},
			// 	onUploadProgress: function( progressEvent ) {
			// 		let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
			// 		this.commit('setting/filePercentage', uploadPercentage)
			// 	}.bind(this)
			// })
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editVersionControl ({ commit }, payload) {
		// console.log(payload)
		let formData = new FormData()
		formData.append('id', payload.id)
		formData.append('name', payload.name)
		formData.append('category', payload.category)
		formData.append('position', payload.position)
		formData.append('description', payload.description)
		formData.append('link_image', payload.link_image)
		formData.append('organization_id', payload.organization_id)
		formData.append('image_url', payload.image_url)
		formData.append('images', payload.images)
		return await this.$axios.post('cms/banners/update', formData,
			{
				headers: {
					'Content-Type': 'multipart/form-data'
				},
				onUploadProgress: function( progressEvent ) {
					let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
					this.commit('setting/filePercentage', uploadPercentage)
				}.bind(this)
			})
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editIsActiveVersionControl ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/banners/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteVersionControl ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('cms/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteVersionControl ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/banners/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	clearData(state){
		state.tempImageFile = null
		state.dataVersionControl = null
	},
	updateStatusVersionControl(state, payload){
		state.statusBanner = payload
	},
	filePercentage(state, payload){
		state.fileProgressPercentage = payload
	},
	savedStateAddVersionControl(state, payload){
		// console.log(payload)
		state.dataVersionControl = payload
	},
	setTempImageFile(state, payload){
		state.tempImageFile = payload
		state.dataVersionControl.image_url = payload.name
		// console.log(state.tempImageFile)
	},
	setTempImageFileVersionControl(state, payload){
		state.tempImageFile = payload
		state.dataVersionControl.image = payload.name
		// console.log(state.tempImageFile)
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},
}


