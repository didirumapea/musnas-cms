
// import {showNotification} from '../plugins/global-function'
export const state = () => ({
	data: null,
	ramUsage: 0,
	diskUsage: 0,
	totalCpu: 0,
	cpuUsage: 0
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		console.log('server init')
	},
	//region BANNER
	async getDashboard ({ commit }) {
		return await this.$axios.$get(`cms/dashboard/preview`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	setMonitor(state, payload){
		// console.log(100 - payload.memory.freeMemPercentage)
		state.ramUsage = 100 - payload.memory.freeMemPercentage
		state.diskUsage = parseFloat(payload.diskDrive.usedPercentage)
		state.totalCpu = payload.cpuCount
		state.cpuUsage = payload.cpuUsage
		// console.log(payload)
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},
}


