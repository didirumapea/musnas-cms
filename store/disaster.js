// import config from '~/store/config'
import {encryptAPI, decryptAPI, showNotification} from '../plugins/global-function'
export const state = () => ({
	tempImageFile: null,
	dataAddDisaster: null,
	fileProgressPercentage: 0,
	statusDisaster: 'add',
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
	async nuxtServerInit({ commit }, { req }){
		// commit()
		console.log('server init')
	},
	//region DISASTER
	async getListDisaster ({ commit }, payload) {
		return await this.$axios.$get(`cms/disaster/list/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			// return await this.$axios.$get(`cms/masterdata/content/list/page=1/limit=10/column-sort=id/sort=desc`)
			.then(response => {
				// console.log(response)
				// commit('setContent', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					// console.log(err.response.data)
					return err.response.data
				}
			})
	},
	async getListSearchDisaster ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.$get(`cms/disaster/list/search-global=${payload.searchVal}/page=${payload.page}/limit=${payload.perPage}/column-sort=${payload.sort.field}/sort=${payload.sort.type}`)
			.then((response) => {
				// console.log(response)
				// commit('setlistVoucherNotUsed', response)
				return response
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async addDisaster ({ commit }, payload) {
		// console.log(this.state.auth.user)
		let formData = new FormData()
		formData.append('name', payload.name)
		formData.append('image_name', null)
		formData.append('exclude_province_area', `{"name": [1, 2, 3, 4]}`)
		formData.append('image', payload.images)
		// formData.append('disaster_id', payload.disaster_id)
		// formData.append('article_type_id', payload.article_type_id)
		// formData.append('title', payload.title)
		// formData.append('content', payload.content)
		// formData.append('publish_date', payload.publish_date)
		return await this.$axios.post('cms/disaster/add', formData)
			.then((response) => {
				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async editDisaster ({ commit, state }, payload) {
		let formData = new FormData()
		formData.append('id', payload.id)
		formData.append('name', payload.name)
		formData.append('image_name', payload.image_url)
		formData.append('exclude_province_area', `{"name": [1, 2, 3, 4]}`)
		formData.append('image', payload.images)
		return await this.$axios.post('cms/disaster/update', formData,
		{
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			onUploadProgress: function( progressEvent ) {
				let uploadPercentage = parseInt( Math.round( ( progressEvent.loaded / progressEvent.total ) * 100 ));
				this.commit('setting/filePercentage', uploadPercentage)
			}.bind(this)
		})
	.then((response) => {
			// console.log()
			return response.data
		})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
					return {
						success: false,
						message: err,
						info: 'some error'
					}
				}
			})
	},
	async editIsActiveDisaster ({ commit }, payload) {
		// console.log(payload)
		let data = {
			"id": payload.id,
			"is_deleted": payload.is_deleted,
		}
		return await this.$axios.post('cms/disaster/update-is-active', data)
			.then((response) => {
				// console.log(response.data)
				if (response.data.success){
					showNotification(response.data.message, false, 'success')
				}else{
					showNotification(response.data.message, false, 'danger')
				}

				// console.log(response)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async safeDeleteDisaster ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.post('masterdata/content/safe-delete', {id: payload, delStatus: '1'})
			.then((response) => {
				// console.log(response.data)
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	async deleteDisaster ({ commit }, payload) {
		// console.log(payload)
		return await this.$axios.delete('cms/disaster/delete/'+payload)
			.then((response) => {
				showNotification(response.data.message, false, 'success')
				return response.data
			})
			.catch(err => {
				if (err.response === undefined){
					return {
						success: false,
						message: err,
						info: 'no connection'
					}
					// console.log('No Connection')
				}else{
					console.log(err.response)
				}
			})
	},
	//endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
	clearDataDisaster(state){
		state.tempImageFile = null
		state.dataAddDisaster = null
	},
	filePercentage(state, payload){
		state.fileProgressPercentage = payload
	},
	updateStatusDisaster(state, payload){
		state.statusDisaster = payload
	},
	savedStateAddDisaster(state, payload){
		// console.log(payload)
		state.dataAddDisaster = payload
	},
	setTempImageFile(state, payload){
		state.tempImageFile = payload
		state.dataAddDisaster.image_url = payload.name
		// console.log(state.tempImageFile)
	},
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
	listCategory: state => {
		return state.listCategory
	},

}


