import createPersistedState from 'vuex-persistedstate'

export default ({ store, isHMR }) => {
	if (isHMR) return

	// window.onNuxtReady(() => {
		createPersistedState({
			key: 'bumikita2020', // key
			// paths: ['auth.count']
			paths: ['klausul_disclaimer', 'auth', 'helper.name', 'settings', 'masterdata', 'content', 'organization', 'banner', 'disaster'] // store file name
		})(store)
	// })
}
