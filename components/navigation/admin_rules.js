import { scHelpers, scMq } from "~/assets/js/utils";
const { uniqueID } = scHelpers;

export const adminRulesEntries = [
	{
		id: uniqueID(),
		title: "Admin",
		icon: "mdi mdi-account-tie",
		page: "/admin"
	},
];
